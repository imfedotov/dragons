package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.service.DragonService;

import java.util.function.Consumer;


public abstract class DragonServiceCommand<T> extends Command {
    protected DragonService dragonService;
    protected Consumer<T> resultCallback;

    DragonServiceCommand(String name) {
        this(name, new Class[0]);
    }

    DragonServiceCommand(String name, Class<?>[] argTypes) {
        super(name, argTypes);
    }

    public void setDragonService(DragonService dragonService) {
        this.dragonService = dragonService;
    }

    public void setResultCallback(Consumer<T> resultCallback) {
        this.resultCallback = resultCallback;
    }
}
