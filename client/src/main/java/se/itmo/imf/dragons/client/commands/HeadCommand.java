package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;

public class HeadCommand extends DragonServiceCommand<Dragon> {
    HeadCommand() {
        super("head");
    }

    public void execute() {
        result = dragonService.head();
    }
}