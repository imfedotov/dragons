package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.core.entities.DragonsInfo;

public class InfoCommand extends DragonServiceCommand<DragonsInfo> {
    InfoCommand() {
        super("info");
    }

    @Override
    public void execute() {
        result = dragonService.getInfo();
    }
}