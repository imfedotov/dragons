package se.itmo.imf.dragons.client.commands;

public class RemoveByIdCommand extends DragonServiceCommand<Boolean> {
    RemoveByIdCommand() {
        super("remove_by_id", new Class[]{Long.class});
    }

    @Override
    public void execute() {
        result = dragonService.remove((Long) args[0]);
    }
}
