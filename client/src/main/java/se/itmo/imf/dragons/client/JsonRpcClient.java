package se.itmo.imf.dragons.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import se.itmo.imf.dragons.client.commands.*;
import se.itmo.imf.dragons.core.entities.DragonsInfo;
import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.jsonrpcv1.JsonRpcRequest;
import se.itmo.imf.dragons.jsonrpcv1.JsonRpcResponse;
import se.itmo.imf.dragons.service.DragonService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Random;

public class JsonRpcClient implements Client {
    private static final Random idRand = new Random();
    private static final ObjectMapper jsonMapper;
    static {
        jsonMapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .build();
    }

    private final CommandFactory commandFactory = new RemoteCommandFactory();

    private String host;
    private int port;
    private String username;
    private String password;

    public JsonRpcClient() {}
    public JsonRpcClient(String host, int port) {
        this.setHostPort(host, port);
    }

    @Override
    public void setHostPort(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public void ping() {
        call("ping", String.class);
    }

    @Override
    public void setCreds(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public void testCreds() {
        call("sayHello", String.class);
    }

    @Override
    public void register() {
        call("register", Void.class, username, password);
    }

    public DragonService getDragonService() {
        return new RemoteDragonService(this);
    }

    @Override
    public CommandFactory getCommandFactory() {
        return commandFactory;
    }

    public <T> T call(String methodName, Class<T> returnType, Object... args) {
        JsonRpcRequest req = new JsonRpcRequest();
        req.setId(idRand.nextInt());
        req.setMethod(methodName);
        req.setParams(args);
        if (username != null && password != null) {
            JsonRpcRequest.Auth auth = new JsonRpcRequest.Auth();
            auth.setUsername(username);
            auth.setPassword(password);
            req.setAuth(auth);
        }

        String serializedReq;
        try {
            serializedReq = jsonMapper.writeValueAsString(req);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        String responseStr;
        try ( Socket sock = new Socket(InetAddress.getByName(host), port) ) {
            PrintWriter pw = new PrintWriter(sock.getOutputStream());
            pw.println(serializedReq);
            pw.flush();
            responseStr = new BufferedReader(new InputStreamReader(sock.getInputStream())).readLine();
        } catch (IOException e) {
            throw new RuntimeException("Server is temporary unavailable");
        }

        JsonRpcResponse response = parseResponse(responseStr, returnType);

        if (response.getError() != null) {
            // System.out.println(response);
            throw new RuntimeException(response.getError());
        }
        return (T) response.getResult();
    }

    private JsonRpcResponse parseResponse(String responseStr, Class<?> returnType) {
        int id;
        String error;
        Object result;
        try {
            JsonNode root = jsonMapper.readTree(responseStr);

            id = root.get("id").asInt();
            error = root.get("error").isNull() ? null : root.get("error").asText();

            JsonNode resultNode = root.get("result");
            if (returnType.isArray()) {
                result = jsonMapper.readerForArrayOf(returnType.componentType()).readValue(resultNode);
            } else {
                result = jsonMapper.treeToValue(resultNode, returnType);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println(result);

        JsonRpcResponse resp = new JsonRpcResponse();
        resp.setId(id);
        resp.setResult(result);
        resp.setError(error);
        return resp;
    }

//    private static Object tryParse(JsonNode node, Class<?> type) {
//        try {
//            return jsonMapper.treeToValue(node, type);
//        } catch (JsonProcessingException e) {
//            return null;
//        }
//    }

    private class RemoteCommandFactory extends DefaultCommandFactory {
        @Override
        public Command newCommand(String name) throws NoSuchCommandException {
            Command cmd = super.newCommand(name);
            if (cmd instanceof DragonServiceCommand) {
                ((DragonServiceCommand<?>) cmd).setDragonService(getDragonService());
            }
            return cmd;
        }
    }
}
