package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.core.entities.dragon.DragonType;

public class FilterByTypeCommand extends DragonServiceCommand<Iterable<Dragon>> {
    FilterByTypeCommand() {
        super("filter_by_type", new Class[]{DragonType.class});
    }

    public void execute() {
        result = dragonService.filterByType((DragonType) args[0]);
    }
}
