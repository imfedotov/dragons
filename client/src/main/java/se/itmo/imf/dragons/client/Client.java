package se.itmo.imf.dragons.client;

import se.itmo.imf.dragons.client.commands.CommandFactory;
import se.itmo.imf.dragons.service.DragonService;

public interface Client {
    DragonService getDragonService();

    CommandFactory getCommandFactory();

    void setHostPort(String host, int port);

    void ping();

    void setCreds(String username, String password);

    void testCreds();

    void register();
}
