package se.itmo.imf.dragons.client.commands;

public class NoSuchCommandException extends RuntimeException {
    private final String commandName;

    NoSuchCommandException(String name) {
        commandName = name;
    }

    public String getCommandName() {
        return commandName;
    }

    @Override
    public String getMessage() {
        return "Command not found: " + commandName;
    }
}

