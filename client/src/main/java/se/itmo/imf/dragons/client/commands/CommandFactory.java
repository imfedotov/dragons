package se.itmo.imf.dragons.client.commands;


public interface CommandFactory {
    Command newCommand(String name);
}
