package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;

public class AddIfMinCommand extends DragonServiceCommand<Dragon> {
    public AddIfMinCommand() {
        super("add_if_min", new Class[]{Dragon.class});
    }

    public void execute() {
        result = dragonService.addIfMin((Dragon) args[0]);
    }
}
