package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;

public class AddCommand extends DragonServiceCommand<Dragon> {
    AddCommand() {
        super("add", new Class[]{Dragon.class});
    }

    @Override
    public void execute() {
        Dragon result = dragonService.add((Dragon) args[0]);
//        resultCallback.accept(result);
        this.result = result;
    }
}


