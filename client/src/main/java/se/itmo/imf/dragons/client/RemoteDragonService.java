package se.itmo.imf.dragons.client;

import se.itmo.imf.dragons.core.entities.DragonsInfo;
import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.core.entities.dragon.DragonHead;
import se.itmo.imf.dragons.core.entities.dragon.DragonType;
import se.itmo.imf.dragons.service.DragonService;

import java.util.List;

public class RemoteDragonService implements DragonService {
    JsonRpcClient client;

    RemoteDragonService(JsonRpcClient client) {
        this.client = client;
    }

    @Override
    public DragonsInfo getInfo() {
        return client.call("getInfo", DragonsInfo.class);
    }

    @Override
    public List<Dragon> getAll() {
        return List.of(client.call("getAll", Dragon[].class));
    }

    @Override
    public Dragon add(Dragon newDragon) {
        return client.call("add", Dragon.class, newDragon);
    }

    @Override
    public Dragon update(long id, Dragon newDragon) {
        return client.call("update", Dragon.class, id, newDragon);
    }

    @Override
    public Dragon remove(long id) {
        return client.call("remove", Dragon.class, id);
    }

    @Override
    public void clear() {
        client.call("clear", Void.class);
    }

    @Override
    public Dragon head() {
        return client.call("head", Dragon.class);
    }

    @Override
    public Dragon removeHead() {
        return client.call("removeHead", Dragon.class);
    }

    @Override
    public Dragon addIfMin(Dragon newDragon) {
        return client.call("addIfMin", Dragon.class, newDragon);
    }

    @Override
    public long countLessThanDescription(String description) {
        return client.call("countLessThanDescription", Long.class, description);
    }

    @Override
    public List<Dragon> filterByType(DragonType dragonType) {
        return List.of(client.call("filterByType", Dragon[].class, dragonType));
    }

    @Override
    public List<Dragon> filterLessThanHead(DragonHead dragonHead) {
        return List.of(client.call("filterLessThanHead", Dragon[].class, dragonHead));
    }
}
