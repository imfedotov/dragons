package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;

public class RemoveHeadCommand extends DragonServiceCommand<Dragon> {
    RemoveHeadCommand() {
        super("remove_head");
    }

    public void execute() {
        result = dragonService.removeHead();
    }
}
