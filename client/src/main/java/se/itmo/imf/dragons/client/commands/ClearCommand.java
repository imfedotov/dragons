package se.itmo.imf.dragons.client.commands;

public class ClearCommand extends DragonServiceCommand<Boolean> {
    ClearCommand() {
        super("clear");
    }

    public void execute() {
        dragonService.clear();
        result = true;
    }
}
