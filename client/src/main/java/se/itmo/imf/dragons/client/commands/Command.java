package se.itmo.imf.dragons.client.commands;


public abstract class Command {
    private final String name;
    private final Class<?>[] argTypes;

    protected Object[] args;
    protected Object result = null;

    protected Command(String name) {
        this(name, new Class[0]);
    }

    protected Command(String name, Class<?>[] argTypes) {
        this.name = name;
        this.argTypes = argTypes;
    }

    public String getName() {
        return name;
    }

    public Class<?>[] getArgTypes() {
        return argTypes;
    }

    public void setArgs(Object... args) {
        checkArgTypes(args);
        this.args = args;
    }

    public Object getResult() {
        return result;
    }

    // The Method.
    public abstract void execute();

    // TODO: throw errors!
    // example  CommandArgumentsException: args[0] is not of type Integer
    private boolean checkArgTypes(Object... args) {
        Class<?>[] argTypes = getArgTypes();

        if (args.length != argTypes.length) {
            return false;
        }

        for (int i = 0; i < args.length; i++) {
            if (!argTypes[i].isAssignableFrom(args[i].getClass())) {
                return false;
            }
        }

        return true;
    }
}

