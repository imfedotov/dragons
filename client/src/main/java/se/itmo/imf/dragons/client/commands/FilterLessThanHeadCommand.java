package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.core.entities.dragon.DragonHead;

public class FilterLessThanHeadCommand extends DragonServiceCommand<Iterable<Dragon>> {
    FilterLessThanHeadCommand() {
        super("filter_less_than_head", new Class[]{DragonHead.class});
    }

    public void execute() {
        result = dragonService.filterLessThanHead((DragonHead) args[0]);
    }
}
