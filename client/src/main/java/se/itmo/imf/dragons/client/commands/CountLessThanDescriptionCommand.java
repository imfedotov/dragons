package se.itmo.imf.dragons.client.commands;

public class CountLessThanDescriptionCommand extends DragonServiceCommand<Long> {
    CountLessThanDescriptionCommand() {
        super("count_less_than_description", new Class[]{String.class});
    }

    public void execute() {
        result = dragonService.countLessThanDescription((String) args[0]);
    }
}
