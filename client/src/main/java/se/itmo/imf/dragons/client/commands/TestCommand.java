package se.itmo.imf.dragons.client.commands;


import se.itmo.imf.dragons.core.entities.dragon.Color;
import se.itmo.imf.dragons.core.entities.dragon.DragonHead;

public class TestCommand extends Command {

    public static class TestEntity {
        private long id;
        private String desc;
        private Color color;
        private DragonHead dragonHead;

        public void setId(long id) {
            this.id = id;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        @Override
        public String toString() {
            return "TestEntity{" +
                    "id=" + id +
                    ", desc='" + desc + '\'' +
                    ", color=" + color +
                    ", dragonHead=" + dragonHead +
                    '}';
        }

        public DragonHead getDragonHead() {
            return dragonHead;
        }

        public void setDragonHead(DragonHead dragonHead) {
            this.dragonHead = dragonHead;
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }
    }

    TestCommand() {
        super("test", new Class[]{TestEntity.class});
    }

    public void execute() {
        System.err.println(args[0]);
        //System.err.println(args[2]);
    }
}
