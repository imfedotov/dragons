package se.itmo.imf.dragons.client.commands;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * This class is used to instantiate Commands by their name.
 * New command instances are empty by default, should be set
 * up with appropriate Receivers and arguments.
 */
public class DefaultCommandFactory implements CommandFactory {
    private static final HashMap<String, Class<? extends Command>> commandsMap;

    static {
        commandsMap = new HashMap<>() {{
            put(new AddCommand().getName(), AddCommand.class);
            put(new AddIfMinCommand().getName(), AddIfMinCommand.class);
            put(new ClearCommand().getName(), ClearCommand.class);
            put(new CountLessThanDescriptionCommand().getName(), CountLessThanDescriptionCommand.class);
//            put(new ExecuteScriptCommand().getName(), ExecuteScriptCommand.class);
//            put(new ExitCommand().getName(), ExitCommand.class);
            put(new FilterByTypeCommand().getName(), FilterByTypeCommand.class);
            put(new FilterLessThanHeadCommand().getName(), FilterLessThanHeadCommand.class);
            put(new HeadCommand().getName(), HeadCommand.class);
//            put(new HelpCommand().getName(), HelpCommand.class);
            put(new InfoCommand().getName(), InfoCommand.class);
            put(new RemoveByIdCommand().getName(), RemoveByIdCommand.class);
            put(new RemoveHeadCommand().getName(), RemoveHeadCommand.class);
            // Synthetic command!
//            put(new SaveCommand().getName(), SaveCommand.class);
            put(new ShowCommand().getName(), ShowCommand.class);
            put(new UpdateCommand().getName(), UpdateCommand.class);
            put(new TestCommand().getName(), TestCommand.class);
        }};
    }

    public Command newCommand(String name) throws NoSuchCommandException {
        Class<? extends Command> cmdCls = commandsMap.get(name);

        if (cmdCls == null) {
            throw new NoSuchCommandException(name);
        }

        try {
            var constructor = cmdCls.getDeclaredConstructor();
            // needed?
            // cons.setAccessible(true);
            return constructor.newInstance();
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("No-arg constructor not found for command " + cmdCls.getSimpleName());
        } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
}
