package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;

public class ShowCommand extends DragonServiceCommand<Iterable<Dragon>> {
    ShowCommand() {
        super("show");
    }

    public void execute() {
        result = dragonService.getAll();
    }
}
