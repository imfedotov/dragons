package se.itmo.imf.dragons.client.commands;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;

public class UpdateCommand extends DragonServiceCommand<Dragon> {
    UpdateCommand() {
        super("update", new Class[]{Long.class, Dragon.class});
    }

    public void execute() {
        result = dragonService.update((Long) args[0], (Dragon) args[1]);
    }
}
