#!/bin/bash

for cls in `cat ./list.txt`; do
    cat > ${cls}Command.java <<EOF
package se.itmo.imf.dragons.client.commands;

public class ${cls}Command extends Command {
    public String getName() {
        return "$cls";
    }
}
EOF
done
