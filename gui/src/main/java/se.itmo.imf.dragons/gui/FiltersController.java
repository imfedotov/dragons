//package se.itmo.imf.dragons.gui;
//
//import java.awt.event.ActionEvent;
//import java.util.logging.Logger;
//
//import javafx.fxml.FXML;
//import javafx.scene.control.*;
//import se.itmo.imf.dragons.core.entities.dragon.DragonHead;
//
//public class FiltersController implements Form<T> {
//
//    @FXML
//    private Button btnCancel;
//
//    @FXML
//    private Button btnConfirm;
//
//    @FXML
//    private ComboBox<?> comboBoxDragonType;
//
//    @FXML
//    private TextArea fieldDescription;
//
//    @FXML
//    private TextField fieldEyesCount;
//
//    @FXML
//    private TextField fieldSize;
//
//    @FXML
//    private ToggleGroup filterGroup;
//
//    @FXML
//    private Label labelEyesCount;
//
//    @FXML
//    private Label labelSize;
//
//    @FXML
//    private RadioButton radioBtnCountLessThanDescription;
//
//    @FXML
//    private RadioButton radioBtnFilterByType;
//
//    @FXML
//    private RadioButton radioBtnFilterLessThanHead;
//
//    private Object result;
//
//    @FXML
//    private void handleRadioBtnCountLessThanDescription(ActionEvent event) {
//        comboBoxDragonType.setDisable(true);
//        fieldSize.setDisable(true);
//        fieldEyesCount.setDisable(true);
//        fieldDescription.setDisable(false);
//    }
//
//    @FXML
//    private void handleRadioBtnFilterByType(ActionEvent event) {
//        fieldSize.setDisable(true);
//        fieldEyesCount.setDisable(true);
//        fieldDescription.setDisable(true);
//        comboBoxDragonType.setDisable(false);
//    }
//
//    @FXML
//    private void handleRadioBtnFilterLessThanHead(ActionEvent event) {
//        fieldDescription.setDisable(true);
//        comboBoxDragonType.setDisable(true);
//        fieldSize.setDisable(false);
//        fieldEyesCount.setDisable(false);
//    }
//
//    @FXML
//    private void handleBtnCancel(ActionEvent event) {
//        Alerts.showInfoAlert("Operation cancelled");
//        btnCancel.getScene().getWindow().hide();
//    }
//
//    @FXML
//    private void handleBtnConfirm(ActionEvent event) {
//        if (filterGroup.getSelectedToggle().equals(radioBtnFilterByType)) {
//            result = comboBoxDragonType.getValue();
//        } else {
//            if (filterGroup.getSelectedToggle().equals(radioBtnCountLessThanDescription)) {
//                result = fieldDescription.getText();
//            } else {
//                DragonHead dragonHead = new DragonHead();
//                dragonHead.setEyesCount(Float.parseFloat(fieldEyesCount.getText()));
//                dragonHead.setSize(Long.parseLong(fieldSize.getText()));
//            }
//        }
//        btnConfirm.getScene().getWindow().hide();
//    }
//    @FXML
//    void initialize() {
//
//
//    }
//
//    @Override
//    public Object getResult() {
//        return
//    }
//
//}
//
