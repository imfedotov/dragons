package se.itmo.imf.dragons.gui;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.ScatterChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import se.itmo.imf.dragons.client.commands.Command;
import se.itmo.imf.dragons.client.commands.CommandFactory;
import se.itmo.imf.dragons.core.entities.DragonsInfo;
import se.itmo.imf.dragons.core.entities.dragon.Color;
import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.core.entities.dragon.DragonType;

import javax.swing.*;
import java.time.ZonedDateTime;
import java.util.List;
//import se.itmo.imf.dragons.gui.Controller;

public class ManagerController {
    // region FXML controls
    @FXML
    private Button btnAdd;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnFilterCount;

    @FXML
    private Button btnHead;

    @FXML
    private Button btnHelp;

    @FXML
    private Button btnInfo;

    @FXML
    private Button btnRemoveHead;

    @FXML
    private Button btnUpdate;

    @FXML
    private TableColumn<Dragon, Long> columnAge;

    @FXML
    private TableColumn<Dragon, Color> columnColor;

    @FXML
    private TableColumn<?, ?> columnCoords;

    @FXML
    private TableColumn<Dragon, Long> columnCoordsX;

    @FXML
    private TableColumn<Dragon, Float> columnCoordsY;

    @FXML
    private TableColumn<Dragon, ZonedDateTime> columnCreationDate;

    @FXML
    private TableColumn<Dragon, String> columnCreator;

    @FXML
    private TableColumn<Dragon, String> columnDescription;

    @FXML
    private TableColumn<?, ?> columnDragonHead;

    @FXML
    private TableColumn<Dragon, Float> columnEyesCount;

    @FXML
    private TableColumn<Dragon, Long> columnId;

    @FXML
    private TableColumn<Dragon, String> columnName;

    @FXML
    private TableColumn<Dragon, Long> columnSize;

    @FXML
    private TableColumn<Dragon, DragonType> dragonType;

    @FXML
    private Label labelCurrentUsername;

    @FXML
    private ComboBox<String> menuLocale;

    @FXML
    private ScrollPane paneCollection;

    @FXML
    private Tab tabDragonCollection;

    @FXML
    private Tab tabGraphics;

    @FXML
    private TableView<Dragon> tableDragon;

    @FXML
    private ScatterChart<?, ?> scatterPlot;
    // endregion

//    CustomSelectionModel<MyData> selectionModel = new CustomSelectionModel<>();
//tableView.setSelectionModel(selectionModel);
//    public class SelectionModel<S> extends TableView.TableViewSelectionModel<S> {
//
//        @Override
//        public ObservableList<TablePosition> getSelectedCells() {
//            return null;
//        }
//
//        @Override
//        public boolean isSelected(int row, TableColumn<S, ?> column) {
//            return false;
//        }
//
//        @Override
//        public void select(int row, TableColumn<S, ?> column) {
//
//        }
//
//        @Override
//        public void clearAndSelect(int row, TableColumn<S, ?> column) {
//
//        }
//
//        @Override
//        public void clearSelection(int row, TableColumn<S, ?> column) {
//
//        }
//
//        @Override
//        public void selectLeftCell() {
//
//        }
//
//        @Override
//        public void selectRightCell() {
//
//        }
//
//        @Override
//        public void selectAboveCell() {
//
//        }
//
//        @Override
//        public void selectBelowCell() {
//
//        }
//    }
    private final CommandFactory commandFactory;
    private final ReadOnlyObjectProperty<List<Dragon>> collectionProp;
    private final ObservableList<Dragon> dragonsList;

    public ManagerController(CommandFactory commandFactory, ReadOnlyObjectProperty<List<Dragon>> collectionProp) {
        this.commandFactory = commandFactory;
        this.collectionProp = collectionProp;
        dragonsList = FXCollections.observableArrayList();

        collectionProp.addListener(DragonListDeltaListener.builder()
                .onAdded(dragonsList::addAll)
                .onRemoved(rd -> {
                    dragonsList.removeIf(d -> rd.contains(d.getId()));
                })
                .build());
    }

    @FXML
    private void handleBtnClear(ActionEvent event) {
        Command command = commandFactory.newCommand("clear");
        command.execute();
    }

    @FXML
    private void handleMenuLocale(ActionEvent event) {
        String selectedLanguage = menuLocale.getSelectionModel().getSelectedItem();
        LocaleController.setLanguage(selectedLanguage);
    }

    @FXML
    private void handleBtnInfo(ActionEvent event) {
        Command command = commandFactory.newCommand("info");
        command.execute();
        Alerts.showInfoAlert(((DragonsInfo) command.getResult()).toString());
    }

    @FXML
    private void handleBtnAdd(ActionEvent event) {
        Dragon d = SceneManager.showForm("DragonSet.fxml");
        Command command = commandFactory.newCommand("add");
        System.out.println(d.toString());
        command.setArgs(d);
        command.execute();
        System.out.println(d.getName());
    }

    @FXML
    private void handleAdd(ActionEvent ae) {
        Dragon d = showDragonForm(DragonSetController.Mode.NEW_DRAGON, null);
        if (d == null) {
            return;
        }
        Command command = commandFactory.newCommand("add");
        command.setArgs(d);
        command.execute();
    }

    @FXML
    private void handleAddIfMin(ActionEvent ae) {
        Dragon d = showDragonForm(DragonSetController.Mode.NEW_DRAGON, null);
        if (d == null) {
            return;
        }
        Command command = commandFactory.newCommand("add_if_min");
        command.setArgs(d);
        command.execute();
    }

    @FXML
    private void handleFilterByHead(ActionEvent ae) {
        Dragon d = showDragonForm(DragonSetController.Mode.FILTER_HEAD, null);
        if (d == null) {
            return;
        }
        Command command = commandFactory.newCommand("filter_less_than_head");
        command.setArgs(d.getHead());
        command.execute();
    }

    @FXML
    private void handleFilterByType(ActionEvent ae) {
        Dragon d = showDragonForm(DragonSetController.Mode.FILTER_TYPE, null);
        if (d == null) {
            return;
        }
        Command command = commandFactory.newCommand("filter_by_type");
        command.setArgs(d.getType());
        command.execute();
    }

    @FXML
    private void handleCountByDesc(ActionEvent ae) {
        Dragon d = showDragonForm(DragonSetController.Mode.COUNT_DESC, null);
        if (d == null) {
            return;
        }
        Command command = commandFactory.newCommand("count_less_than_description");
        command.setArgs(d.getDescription());
        command.execute();
    }

    private Dragon showDragonForm(DragonSetController.Mode mode, Dragon prevDragon) {
        return SceneManager.showForm("DragonSet.fxml", (clazz) -> {
            DragonSetController ctrl = new DragonSetController();
            ctrl.setMode(mode);
            ctrl.setDragon(prevDragon);
            return ctrl;
        });
    }

    @FXML
    void initialize() {
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnName.setCellValueFactory(new PropertyValueFactory<>("name"));

//        columnCoordsX.setCellValueFactory(new PropertyValueFactory<>("coordsX"));
//        columnCoordsY.setCellValueFactory();

        columnCreationDate.setCellValueFactory(new PropertyValueFactory<>("creationDate"));
        columnAge.setCellValueFactory(new PropertyValueFactory<>("age"));
        columnDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        columnColor.setCellValueFactory(new PropertyValueFactory<>("color"));
        dragonType.setCellValueFactory(new PropertyValueFactory<>("type"));

//        columnSize.setCellValueFactory(new PropertyValueFactory<>("size"));
//        columnEyesCount.setCellValueFactory(new PropertyValueFactory<>("eyesCount"));

        columnCreator.setCellValueFactory(new PropertyValueFactory<>("creator"));

        tableDragon.setRowFactory((TableView<Dragon> table) -> {
            TableRow<Dragon> row = new TableRow<>();
            ContextMenu menu = new ContextMenu();
            MenuItem remove = new MenuItem("Remove");
            remove.setOnAction((ae) -> {
                System.out.println("Deleting dragon!");
                Command cmd = commandFactory.newCommand("remove_by_id");
                cmd.setArgs(row.getItem().getId());
                cmd.execute();
            });
            menu.getItems().add(remove);

            MenuItem update = new MenuItem("Update");
            update.setOnAction((ae) -> {
                Dragon d = showDragonForm(DragonSetController.Mode.UPDATE_DRAGON, row.getItem());
                if (d == null) {
                    return;
                }
                Command cmd = commandFactory.newCommand("update");
                cmd.setArgs(d.getId(), d);
                cmd.execute();
            });
            menu.getItems().add(update);

            row.contextMenuProperty().bind(
                Bindings.when(row.emptyProperty())
                        .then((ContextMenu) null)
                        .otherwise(menu)
            );
            return row;
        });

        tableDragon.setItems(dragonsList);

        ObservableList<String> optionsQuality = FXCollections.observableArrayList();
        optionsQuality.add("Russian");
        optionsQuality.add("Spanish");
        optionsQuality.add("Dutch");
        //optionsQuality.add("Испанский");

        menuLocale.setItems(optionsQuality);

        DragonsCanvas canvas = new DragonsCanvas(collectionProp);
        AnchorPane anchorPane = new AnchorPane(canvas);
        anchorPane.autosize();
        canvas.initialize();

        // Add the DragonsCanvas to the content of tabGraphics
        tabGraphics.setContent(anchorPane);

        // Add an event handler to the selection property of tabGraphics
        tabGraphics.selectedProperty().addListener((observable, oldValue, newValue) -> {
        });


    }
}

