package se.itmo.imf.dragons.gui;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.util.Duration;
import se.itmo.imf.dragons.client.Client;
import se.itmo.imf.dragons.client.JsonRpcClient;

public class GuiMain {
    public static void main(String... args) {
        Application.launch(JavaFXApp.class, args);
    }

    public static class JavaFXApp extends Application {
        private Client client;

        @Override
        public void init() throws Exception {
            super.init();
            this.client = new JsonRpcClient();
        }

        @Override
        public void start(Stage stage) throws Exception {
            SceneManager.setMainStage(stage);

            CollectionService svc = new CollectionService(client.getDragonService());
            SceneManager.showMain("Manager.fxml", clazz -> {
                return new ManagerController(client.getCommandFactory(), svc.lastValueProperty());
            });

            boolean result = SceneManager.showForm("Auth.fxml", clazz -> {
                AuthController ac = new AuthController();
                ac.setClient(client);
                return ac;
            });

            svc.setPeriod(Duration.seconds(1));
            svc.start();

            System.out.println("Login form result: " + result);
        }
    }
}
