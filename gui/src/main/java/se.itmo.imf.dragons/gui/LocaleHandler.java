package se.itmo.imf.dragons.gui;

import java.util.ResourceBundle;

public class LocaleHandler {
    public static final ResourceBundle messages = LocaleController.getBundle();
    public static String btnRegister() {return messages.getString("btnRegister");}
    public static String fieldLogin() {return messages.getString("fieldLogin");}
    public static String btnLogin() {return messages.getString("btnLogin");}
    public static String labelGreeting() {return messages.getString("labelGreeting");}
    public static String fieldPassword() {return messages.getString("fieldPassword");}
    public static String menuLanguage() {return messages.getString("menuLanguage");}
    public static String labelName() {return messages.getString("labelName");}
    public static String fieldName() {return messages.getString("fieldName");}
    public static String labelAge() {return messages.getString("labelAge");}
    public static String fieldAge() {return messages.getString("fieldAge");}
    public static String labelDescription() {return messages.getString("labelDescription");}
    public static String fieldDescription() {return messages.getString("fieldDescription");}
    public static String menuDragonType() {return messages.getString("menuDragonType");}
    public static String menuColor() {return messages.getString("menuColor");}
    public static String labelColor() {return messages.getString("labelColor");}
    public static String labelDragonType() {return messages.getString("labelDragonType");}
    public static String labelSize() {return messages.getString("labelSize");}
    public static String labelEyesCount() {return messages.getString("labelEyesCount");}
    public static String fieldSize() {return messages.getString("fieldSize");}
    public static String fieldEyesCount() {return messages.getString("fieldEyesCount");}
    public static String labelDragonHead() {return messages.getString("labelDragonHead");}
    public static String labelCoordsX() {return messages.getString("labelCoordsX");}
    public static String labelCoordsY() {return messages.getString("labelCoordsY");}
    public static String fieldCoordsX() {return messages.getString("fieldCoordsX");}
    public static String fieldCoordsY() {return messages.getString("fieldCoordsY");}
    public static String labelCoords() {return messages.getString("labelCoords");}
    public static String tabDragonCollection() {return messages.getString("tabDragonCollection");}
    public static String tabGraphics() {return messages.getString("tabGraphics");}
    public static String alertData() {return messages.getString("alertData");}
    public static String alertAuthFailed() {return messages.getString("alertAuthFailed");}
    public static String alertRegFailed() {return messages.getString("alertRegFailed");}
    public static String alertSuccess() {return messages.getString("alertSuccess");}
//    public static String () {return messages.getString("");}
//    public static String () {return messages.getString("");}
//    public static String () {return messages.getString("");}
//    public static String () {return messages.getString("");}
//    public static String () {return messages.getString("");}
//    public static String () {return messages.getString("");}
//    public static String () {return messages.getString("");}
}
