package se.itmo.imf.dragons.gui;


import javafx.beans.value.ObservableValue;
import se.itmo.imf.dragons.core.entities.dragon.Dragon;

import javafx.beans.value.ChangeListener;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class DragonListDeltaListener implements ChangeListener<List<Dragon>> {
    private Consumer<List<Dragon>> addedCallback, updatedCallback;
    private Consumer<Set<Long>> removedCallback;

    @Override
    public void changed(ObservableValue<? extends List<Dragon>> observable,
                        List<Dragon> oldValue, List<Dragon> newValue) {
        List<Dragon> oldValue1;
        if (oldValue == null) {
            oldValue1 = List.<Dragon>of();
        } else {
            oldValue1 = oldValue;
        }

        Set<Long> oldIds = oldValue1.stream().map(Dragon::getId).collect(Collectors.toSet());
        Set<Long> newIds = newValue.stream().map(Dragon::getId).collect(Collectors.toSet());

        List<Dragon> added = newIds.stream()
                .filter(id -> !oldIds.contains(id))
                .map(
                        id -> newValue.stream().filter(d -> d.getId() == id).findFirst().get()
                ).toList();
        List<Dragon> updated = newIds.stream()
                .filter(id -> oldIds.contains(id))
                .map(
                        id -> newValue.stream().filter(d -> d.getId() == id).findFirst().get()
                ).toList();
        Set<Long> removedIds = oldIds.stream()
                .filter(id -> !newIds.contains(id)).collect(Collectors.toSet());

        if (addedCallback != null) {
            addedCallback.accept(added);
        }
        if (removedCallback != null) {
            removedCallback.accept(removedIds);
        }
        if (updatedCallback != null) {
            updatedCallback.accept(updated);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    private DragonListDeltaListener() { }

    public static class Builder {
        private final DragonListDeltaListener instance;

        Builder() {
            instance = new DragonListDeltaListener();
        }

        public DragonListDeltaListener build() {
            return instance;
        }

        public Builder onAdded(Consumer<List<Dragon>> callback) {
            instance.addedCallback = callback;
            return this;
        }

        public Builder onUpdated(Consumer<List<Dragon>> callback) {
            instance.updatedCallback = callback;
            return this;
        }

        public Builder onRemoved(Consumer<Set<Long>> callback) {
            instance.removedCallback = callback;
            return this;
        }
    }
}
