package se.itmo.imf.dragons.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;

public class SceneManager {
    private static Stage mainStage;

    public static void setMainStage(Stage stage) {
        mainStage = stage;
    }

    public static void showMain(String fxmlPath) {
        showMain(fxmlPath, null);
    }

    public static void showMain(String fxmlPath, Callback<Class<?>, Object> ctrlFactory) {
        show(mainStage, fxmlPath, ctrlFactory);
    }

    public static <T> T showForm(String fxmlPath) {
        return showForm(fxmlPath, null);
    }

    public static <T> T showForm(String fxmlPath, Callback<Class<?>, Object> ctrlFactory) {
        Stage stage = new Stage();
        stage.initOwner(mainStage);
        stage.setResizable(false);
        stage.setFullScreen(false);
        stage.setMaximized(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        Form<T> form = (Form<T>) show(stage, fxmlPath, ctrlFactory);
        return form.getResult();
    }

    private static Object show(Stage stage, String fxmlPath, Callback<Class<?>, Object> ctrlFactory) {
        FXMLLoader loader = new FXMLLoader(SceneManager.class.getResource(fxmlPath));
        if (ctrlFactory != null) {
            loader.setControllerFactory(ctrlFactory);
        }
        Scene scene;
        try {
            scene = loader.load();
        } catch (IOException e) {
            throw new RuntimeException("Cannot load stage " + fxmlPath, e);
        }
        stage.setScene(scene);

        if (stage == mainStage) {
            stage.show();
        } else {
            stage.showAndWait();
        }

        return loader.getController();
    }
}

