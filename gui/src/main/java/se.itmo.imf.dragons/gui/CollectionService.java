package se.itmo.imf.dragons.gui;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.service.DragonService;

import java.util.List;

public class CollectionService extends ScheduledService<List<Dragon>> {
    private final DragonService dragonService;

    public CollectionService(DragonService dragonService) {
        this.dragonService = dragonService;
    }

    @Override
    protected Task<List<Dragon>> createTask() {
        return new Task<>() {
            @Override
            protected List<Dragon> call() throws Exception {
                System.out.println("Service: polling!");
                return dragonService.getAll();
            }
        };
    }
}
