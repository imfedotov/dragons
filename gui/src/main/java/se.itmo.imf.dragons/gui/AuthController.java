package se.itmo.imf.dragons.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML; import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import se.itmo.imf.dragons.client.Client;

public class AuthController implements Form<Boolean> {
    // region FXML contols
    @FXML private Button btnLogin;

    @FXML private Hyperlink btnRegister;

    @FXML private TextField fieldHost;

    @FXML private TextField fieldLogin;

    @FXML private PasswordField fieldPassword;

    @FXML private TextField fieldPort;

    @FXML private ImageView imgGreeting;

    @FXML private Label labelGreeting;

    @FXML private Label labelGreeting1;

    @FXML private ComboBox<String> menuLanguage;
    // endregion

    private Client client;

    void setClient(Client clientInstance) {
        this.client = clientInstance;
    }

    @Override
    public Boolean getResult() {
        return true;
    }

    private boolean requireFields() {
        boolean ok = true;
        if (fieldHost.getText().isBlank()) {
            ok = false;
        }
        if (fieldPort.getText().isBlank()) {
            ok = false;
        }
        if (fieldLogin.getText().isEmpty()) {
            ok = false;
        }
        if (fieldPassword.getText().isEmpty()) {
            ok = false;
        }
        return ok;
    }

    void setupClient() {
        if (!requireFields()) {
            System.err.println("Fields empty");
            return;
        }

        String host = fieldHost.getText();
        int port = Integer.parseInt(fieldPort.getText());
        client.setHostPort(host, port);
        client.ping();
        String username = fieldLogin.getText();
        String password = fieldPassword.getText();
        client.setCreds(username, password);
    }

    @FXML
    private void handleBtnLogin(ActionEvent event) {
        setupClient();
        client.testCreds();
        btnLogin.getScene().getWindow().hide();
    }

    @FXML
    private void handleBtnRegister(ActionEvent event) {
        setupClient();
        client.register();
    }

    @FXML void initialize() {
        ObservableList<String> optionsQuality = FXCollections.observableArrayList();
        optionsQuality.add("Russian");
        optionsQuality.add("Spanish");
        optionsQuality.add("Dutch");
        //optionsQuality.add("Испанский");
        menuLanguage.setItems(optionsQuality);
    }
}
