package se.itmo.imf.dragons.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import se.itmo.imf.dragons.core.entities.dragon.*;
import se.itmo.imf.dragons.core.entities.dragon.Color;

import java.net.URL;
import java.util.ResourceBundle;

public class DragonSetController implements Form<Dragon> {

    // region FXML controls
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnConfirm;

    @FXML
    private ComboBox<Color> comboBoxColor;

    @FXML
    private ComboBox<DragonType> comboBoxDragonType;

    @FXML
    private TextField fieldCoordsX;

    @FXML
    private TextField fieldCoordsY;

    @FXML
    private TextArea fieldDescription;

    @FXML
    private TextField fieldEyesCount;

    @FXML
    private TextField fieldName;

    @FXML
    private TextField fieldSize;

    @FXML
    private Label labelAge;

    @FXML
    private Label labelColor;

    @FXML
    private Label labelCoords;

    @FXML
    private Label labelCoordsX;

    @FXML
    private Label labelCoordsY;

    @FXML
    private Label labelDescription;

    @FXML
    private Label labelDragonHead;

    @FXML
    private Label labelEyesCount;

    @FXML
    private Label labelName;

    @FXML
    private Label labelSize;

    @FXML
    Spinner<Integer> spinnerAge = new Spinner<>();
    // endregion

    Mode mode;

    Dragon dragon = null;
    Dragon resultDragon = null;

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public void setDragon(Dragon dragon) {
        if (dragon != null) {
            this.dragon = dragon;
            return;
        }
        dragon = new Dragon();
        dragon.setHead(new DragonHead());
        dragon.setCoordinates(new Coordinates());
        this.dragon = dragon;
    }

    @Override
    public Dragon getResult() {
        return resultDragon;
    }

    ObservableList<Color> comboColorValues = FXCollections.observableArrayList(Color.values());
    ObservableList<DragonType> comboTypeValues = FXCollections.observableArrayList(DragonType.values());

    @FXML
    private void handleConfirm(ActionEvent event) {
        System.out.println("DragonForm " + dragon.getName());

        boolean ok = switch (mode) {
            case NEW_DRAGON, UPDATE_DRAGON ->
                    requireFields(fieldName,
                            comboBoxColor, comboBoxDragonType, spinnerAge,
                            fieldCoordsX, fieldCoordsY,
                            fieldSize, fieldEyesCount);
            case FILTER_TYPE ->
                    requireFields(comboBoxDragonType);
            case FILTER_HEAD ->
                requireFields(fieldSize, fieldEyesCount);
            case COUNT_DESC ->
                requireFields(fieldDescription);
        };
        if (!ok) {
            return;
        }

        switch (mode) {
            case NEW_DRAGON, UPDATE_DRAGON -> {
                dragon.setName(fieldName.getText());
                dragon.setAge(spinnerAge.getValue());
                dragon.setDescription(fieldDescription.getText());
                dragon.setColor(comboBoxColor.getValue());
                dragon.setType(comboBoxDragonType.getValue());
                dragon.setHead(new DragonHead());
                dragon.getHead().setSize(Long.parseLong(fieldSize.getText()));
                dragon.getHead().setEyesCount(Float.parseFloat(fieldEyesCount.getText()));
                dragon.setCoordinates(new Coordinates());
                dragon.getCoordinates().setX(Long.parseLong(fieldCoordsX.getText()));
                dragon.getCoordinates().setY(Float.parseFloat(fieldCoordsY.getText()));
            }
            case FILTER_HEAD -> {
                dragon.getHead().setSize(Long.parseLong(fieldSize.getText()));
                dragon.getHead().setEyesCount(Float.parseFloat(fieldEyesCount.getText()));
            }
            case FILTER_TYPE ->
                dragon.setType(comboBoxDragonType.getValue());
            case COUNT_DESC ->
                dragon.setDescription(fieldDescription.getText());
        }

        resultDragon = dragon;
        //System.out.println(dragon);
        btnConfirm.getScene().getWindow().hide();
    }

    @FXML
    private void handleCancel(ActionEvent event) {
        Alerts.showInfoAlert("Operation cancelled");
        btnCancel.getScene().getWindow().hide();
    }

    @FXML
    void initialize() {
        disableControlsForMode();

        if (mode == Mode.UPDATE_DRAGON) {
            fillFields();
        }

        SpinnerValueFactory<Integer> spinnerValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 2147483646);
        spinnerAge.setValueFactory(spinnerValueFactory);
        comboBoxColor.setItems(comboColorValues);
        comboBoxDragonType.setItems(comboTypeValues);
    }

    private void fillFields() {
        fieldName.setText(dragon.getName());
        fieldDescription.setText(dragon.getDescription());
        comboBoxColor.setValue(dragon.getColor());
        comboBoxDragonType.setValue(dragon.getType());
        // spinnerAge.getValueFactory().setValue((int) dragon.getAge());
        fieldCoordsX.setText(Long.toString(dragon.getCoordinates().getX()));
        fieldCoordsY.setText(Float.toString(dragon.getCoordinates().getY()));
//        fieldSize.setText(Long.toString(dragon.getHead().getSize()));
//        fieldEyesCount.setText(Float.toString(dragon.getHead().getSize()));
    }

    private void disableControlsForMode() {
        switch (mode) {
            case FILTER_HEAD ->
                    disableAll(fieldName, fieldDescription,
                            comboBoxColor, comboBoxDragonType, spinnerAge,
                            fieldCoordsX, fieldCoordsY);
            case FILTER_TYPE ->
                    disableAll(fieldName, fieldDescription,
                            comboBoxColor, spinnerAge,
                            fieldCoordsX, fieldCoordsY,
                            fieldSize, fieldEyesCount);
            case COUNT_DESC ->
                    disableAll(fieldName,
                            comboBoxColor, comboBoxDragonType, spinnerAge,
                            fieldCoordsX, fieldCoordsY,
                            fieldSize, fieldEyesCount);
            default -> { }
        }
    }

    private void disableAll(Control... controls) {
        for (var control : controls) {
            control.setDisable(true);
        }
    }

    private boolean requireFields(Control... controls) {
        for (var control : controls) {
            if (control instanceof TextInputControl tic) {
                if (tic.getText() == null || tic.getText().isEmpty()) {
                    return false;
                }
            } else if (control instanceof ComboBox<?> cb) {
                if (cb.getValue() == null) {
                    return false;
                }
            } else if (control instanceof Spinner<?> sp) {
                if (sp.getValue() == null) {
                    return false;
                }
            }
        }
        return true;
    }

    public enum Mode {
        NEW_DRAGON,
        UPDATE_DRAGON,
        FILTER_HEAD,
        FILTER_TYPE,
        COUNT_DESC
    }
}




