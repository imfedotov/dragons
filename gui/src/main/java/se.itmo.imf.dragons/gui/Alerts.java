package se.itmo.imf.dragons.gui;

import javafx.scene.control.Alert;

public class Alerts {
    public static void showInfoAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error title here");
        alert.setContentText(message);
        alert.show();
    }
    public static void showErrAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error title here");
        alert.setContentText(message);
        alert.show();
    }
}
