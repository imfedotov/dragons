package se.itmo.imf.dragons.gui;

import java.util.ResourceBundle;

public class LocaleController {
    private static ResourceBundle bundle;
    public static void setLanguage(String lang) {
        if (lang.equals("Русский")||lang.equals("Russisch")||lang.equals("Ruso")) {
            bundle = ResourceBundle.getBundle("localization_ru.properties");
        }
        if (lang.equals("Нидерландский")||lang.equals("Nederlands")||lang.equals("Holandés")) {
            bundle = ResourceBundle.getBundle("locale_dut.properties");
        }
        if (lang.equals("Испанский")||lang.equals("Spaans")||lang.equals("Español")) {
            bundle = ResourceBundle.getBundle("locale_spa.properties");
        }
    }
    public static ResourceBundle getBundle() {return bundle;}
}
