package se.itmo.imf.dragons.gui;

import javafx.animation.RotateTransition;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import se.itmo.imf.dragons.core.entities.dragon.Dragon;

import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

public class DragonsCanvas extends AnchorPane {

    private static final int WIDTH = 400;
    private static final int HEIGHT = 400;
    private static final int AXIS_WIDTH = 2;
    private static final Color AXIS_COLOR = Color.BLACK;
    private static final int TICK_LENGTH = 5;
    private static final int TICK_SPACING = 10;
    private static final Color TICK_COLOR = Color.BLACK;

    private ObservableList<Dragon> collection;
    ReadOnlyObjectProperty<List<Dragon>> collectionProp;

    public DragonsCanvas(ReadOnlyObjectProperty<List<Dragon>> collectionProp) {
        super();
        this.collectionProp = collectionProp;
        this.collection = FXCollections.observableArrayList();
        //initialize();
    }

    public void setCollection(ObservableList<Dragon> collection) {
        this.collection = collection;
    }

    protected void initialize() {
        Canvas canvas = new Canvas(WIDTH, HEIGHT);
        getChildren().add(canvas);

        AnchorPane.setTopAnchor(canvas, 0.0);
        AnchorPane.setBottomAnchor(canvas, 0.0);
        AnchorPane.setLeftAnchor(canvas, 0.0);
        AnchorPane.setRightAnchor(canvas, 0.0);

        collectionProp.addListener(DragonListDeltaListener.builder()
                .onAdded(this::putDragons)
                .onRemoved(rd -> collection.removeIf(d -> rd.contains(d.getId())))
                .build());

        GraphicsContext gc = canvas.getGraphicsContext2D();

        // Отрисовка осей координат
        gc.setLineWidth(AXIS_WIDTH);
        gc.setStroke(AXIS_COLOR);
        gc.strokeLine(0, HEIGHT / 2, WIDTH, HEIGHT / 2); // Ось X
        gc.strokeLine(WIDTH / 2, 0, WIDTH / 2, HEIGHT); // Ось Y
        // Отрисовка меток на осях
        gc.setLineWidth(1);
        gc.setStroke(TICK_COLOR);
        for (int x = -WIDTH / 2; x < WIDTH / 2; x += TICK_SPACING) {
            gc.strokeLine(x + WIDTH / 2, HEIGHT / 2 - TICK_LENGTH / 2,
                    x + WIDTH / 2, HEIGHT / 2 + TICK_LENGTH / 2);
        }
        for (int y = -HEIGHT / 2; y < HEIGHT / 2; y += TICK_SPACING) {
            gc.strokeLine(WIDTH / 2 - TICK_LENGTH / 2, y + HEIGHT / 2,
                    WIDTH / 2 + TICK_LENGTH / 2, y + HEIGHT / 2);
        }

    }
    void putDragons(List<Dragon> addedDragons) {
        int dragonWidth = 30;
        int dragonHeight = 30;
        for (Dragon dragon : addedDragons) {

            int x = (int) (-dragon.getCoordinates().getX() * TICK_SPACING + WIDTH / 2 - dragonWidth / 2);
            int y = (int) (-dragon.getCoordinates().getY() * TICK_SPACING + HEIGHT / 2 - dragonHeight / 2);
            Random random = new Random();
            int red = random.nextInt(256);
            int green = random.nextInt(256);
            int blue = random.nextInt(256);
            Color dragonColor = Color.rgb(red, green, blue);

            Rectangle dragonShape = new Rectangle(x, y, dragonWidth, dragonHeight);
            dragonShape.setFill(dragonColor);

            getChildren().add(dragonShape);

            RotateTransition rotateTransition = new RotateTransition(Duration.seconds(2), this);
            rotateTransition.setByAngle(0);
            rotateTransition.setCycleCount(RotateTransition.INDEFINITE);
            rotateTransition.setAutoReverse(false);
            rotateTransition.play();
        }
    }
}
