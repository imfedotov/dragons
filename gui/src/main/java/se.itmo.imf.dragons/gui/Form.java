package se.itmo.imf.dragons.gui;

public interface Form<T> {
    T getResult();
}
