package se.itmo.imf.dragons.cli.interpreter;

import se.itmo.imf.dragons.core.entities.dragon.DragonHead;

import java.util.Scanner;

public class DragonHeadReader extends ClassReader<DragonHead> {

    DragonHeadReader(Scanner sc, boolean isInteractive) {
        super(sc, isInteractive);
    }

    public DragonHead read() {
        DragonHead dragonHead = new DragonHead();

        dragonHead.setSize(readField("Size", Long.class, false));
        dragonHead.setEyesCount(readField("Eyes Count", Float.class, false));

        return dragonHead;
    }
}
