package se.itmo.imf.dragons.cli.interpreter;

public final class Messages {
    public static final String GREETING = "Dragons v1.0\n" +
            "(c) Ivan Fedotov <imfedotov@niuitmo.ru> & Vladimir Bessonov <373265@niuitmo.ru>";

    public static final String HELP = """
            Commands: (<> - inline argument; {} - line-by-line input)
            help - show this message
            info - print collection info
            show - print all dragons in the collection
            add {Dragon} - add dragon to the collection, print id
            update <id> {Dragon} - replace dragon in the collection, print id
            remove_by_id <id> - remove dragon from collection
            clear - clear the collection
            save - save collection to file $DRAGONS_JSON
            execute_script <file_name> - executes script
            exit - quit the program without saving
            head - print the first element in the collection
            remove_head - pop and print the first element in the collection
            add_if_min {Dragon} - add element if it's less than minimal already contained
            count_less_than_description <description> - self-explanatory
            filter_by_type <type> - print dragons whose type is same as specified
            filter_less_than_head {Dragon Head} - self-explanatory
            """;

    public static final String BYE = "Bye-bye!";

    public static final String ADDED_F = "Added dragon, id %d\n";
    public static final String UPDATED_F = "Updated dragon with id %d\n";
    public static final String REMOVED_F = "Removed dragon with id %d\n";

    public static final String ENOTFOUND_F = "Dragon with id %d not found\n";
}
