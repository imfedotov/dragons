package se.itmo.imf.dragons.cli.interpreter;

import se.itmo.imf.dragons.core.entities.dragon.Coordinates;

import java.util.Scanner;

public class CoordinatesReader extends ClassReader<Coordinates> {
    CoordinatesReader(Scanner sc, boolean isInteractive) {
        super(sc, isInteractive);
    }

    public Coordinates read() {
        Coordinates coordinates = new Coordinates();

        coordinates.setX(readField("X", Long.class, false));

        float y;
        do {
            y = readField("Y", Float.class, false);
            if (y > -964) {
                coordinates.setY(y);
                break;
            }
            println("Y must be greater than -964");
        } while (true);

        return coordinates;
    }
}
