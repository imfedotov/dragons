package se.itmo.imf.dragons.cli;

import se.itmo.imf.dragons.cli.interpreter.Interpreter;
import se.itmo.imf.dragons.client.Client;
import se.itmo.imf.dragons.client.JsonRpcClient;

import java.io.IOException;

public class Main {
    private static final String ENV_FILE = "DRAGONS_JSON";
    private static final String ENV_SCRIPT = "DRAGONS_SCRIPT";
    private static final String ENV_HOST = "DRAGONS_HOST";
    private static final String ENV_PORT = "DRAGONS_PORT";

    public static void main(String... args) {
        Client client = setupRpcClient();
        Interpreter interpreter = setupInterpreter(client);

        interpreter.run();
    }

//    private static Client setupLocalClient() {
//        if (System.getenv(ENV_FILE) == null) {
//            System.err.printf("Please set $%s.\n", ENV_FILE);
//            System.exit(1);
//        }
//
//        return new LocalClient(System.getenv(ENV_FILE));
//    }

    private static Client setupRpcClient() {
        String host = requireEnv(ENV_HOST);
        int port = Integer.parseInt(requireEnv(ENV_PORT));

        return new JsonRpcClient(host, port);
    }

    static Interpreter setupInterpreter(Client client) {
        String scriptPath = System.getenv(ENV_SCRIPT);
        if (scriptPath == null) {
            return new Interpreter(client);
        } else {
            try {
                return new Interpreter(client, scriptPath);
            } catch (IOException e) {
                System.err.println("IO Exception opening script file: " + e.getMessage());
                System.exit(1);
                return null;
            }
        }
    }

    private static String requireEnv(String varName) {
        String val = System.getenv(varName);
        if (val == null) {
            System.err.printf("Please set %s\n", varName);
            System.exit(1);
        }
        return val;
    }
}