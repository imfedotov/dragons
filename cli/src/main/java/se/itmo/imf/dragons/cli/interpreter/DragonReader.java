package se.itmo.imf.dragons.cli.interpreter;

import se.itmo.imf.dragons.core.entities.dragon.*;

import java.util.Scanner;

public class DragonReader extends ClassReader<Dragon> {
    DragonReader(Scanner sc, boolean isInteractive) {
        super(sc, isInteractive);
    }

    @Override
    public Dragon read() {
        Dragon dragon = new Dragon();

        dragon.setName(readField("Name", String.class, false));

        ClassReader<Coordinates> coordinatesReader = new CoordinatesReader(sc, isInteractive);
        coordinatesReader.setFieldPromptPrefix("Coordinates::");
        dragon.setCoordinates(coordinatesReader.read());

        long age;
        do {
            age = readField("Age", Long.class, false);
            if (age > 0) {
                dragon.setAge(age);
                break;
            }
            println("Age must be greater than 0");
        } while (true);

        dragon.setDescription(readField("Description", String.class, true));

        dragon.setColor(readEnumField("Color", Color.class, false));
        dragon.setType(readEnumField("Type", DragonType.class, false));

        // TODO: nullable this field... sike!
        ClassReader<DragonHead> dragonHeadReader = new DragonHeadReader(sc, isInteractive);
        dragonHeadReader.setFieldPromptPrefix("Dragon Head::");
        dragon.setHead(dragonHeadReader.read());

        return dragon;
    }
}
