package se.itmo.imf.dragons.cli.interpreter;

import se.itmo.imf.dragons.client.commands.TestCommand.TestEntity;
import se.itmo.imf.dragons.core.entities.dragon.Color;

import java.util.Scanner;

public class TestEntityReader extends ClassReader<TestEntity> {

    TestEntityReader(Scanner sc, boolean isInteractive) {
        super(sc, isInteractive);
    }

    @Override
    public TestEntity read() {
        TestEntity testEntity = new TestEntity();

        readField("x Coordinate",
                Long.class,
                false);
        readField("Description",
                String.class,
                true);
        readEnumField("Color",
                Color.class,
                false);

        DragonHeadReader dragonHeadReader = new DragonHeadReader(sc, isInteractive);
        dragonHeadReader.setFieldPromptPrefix("Dragon Head::");

        testEntity.setDragonHead(dragonHeadReader.read());

        return testEntity;
    }

}
