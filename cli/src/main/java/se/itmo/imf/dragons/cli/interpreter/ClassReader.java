package se.itmo.imf.dragons.cli.interpreter;

import java.util.*;
import java.util.function.Function;

abstract class ClassReader<C> {
    protected final Scanner sc;
    protected final boolean isInteractive;

    private String fieldPromptPrefix = "";

    private static final HashMap<Class<?>, Function<Scanner, ?>> typeReaders;

    static {
        typeReaders = new HashMap<>() {{
            put(Integer.class, Scanner::nextInt);
            put(Long.class, Scanner::nextLong);
            put(Float.class, Scanner::nextFloat);
            put(String.class, (scanner) -> {
                return scanner.nextLine();
            });
        }};
    }

    protected ClassReader(Scanner sc, boolean isInteractive) {
        this.sc = sc;
        this.isInteractive = isInteractive;
    }

    public abstract C read();

    public void setFieldPromptPrefix(String fieldPromptPrefix) {
        this.fieldPromptPrefix = fieldPromptPrefix;
    }

    protected <T> T readField(String name, Class<T> cls, boolean nullable) {
        T value;
        Scanner inputLineSc = null;

        while (true) {
            print(fieldPromptPrefix + name
                    + (nullable ? (" (null)") : "")
                    + "> ");

//            try {
            inputLineSc = new Scanner(sc.nextLine());
//            } catch (NoSuchElementException e) {
//                if (nullable)
//                    return null;
//                println("Field cannot be null");
//                continue;
//            }

            Function<Scanner, T> reader = (Function<Scanner, T>) typeReaders.get(cls);
            if (reader == null) {
                throw new RuntimeException("ClassReader: unsupported type " + cls);
            }

            try {
                value = reader.apply(inputLineSc);
            } catch (InputMismatchException e) {
                println("Expected " + cls.getSimpleName());
                continue;
            } catch (NoSuchElementException e) {
                if (nullable) {
                    value = null;
                } else {
                    System.out.println("Field cannot be null");
                    continue;
                }
            }
            if (!nullable && value == null) {
                println("Field " + name + " cannot be null");
                continue;
            }

            if (inputLineSc.hasNext()) {
                println("Trailing input");
                continue;
            }

            break;
        }


        return value;
    }

    protected <T extends Enum<T>> T readEnumField(String name, Class<T> enumClass, boolean nullable) {
        T value;
        Scanner inputLineSc = null;

        while (true) {
            // TODO: enum values
            print(fieldPromptPrefix + name
                    + (nullable ? (" (null)") : "")
                    + "> ");

            inputLineSc = new Scanner(sc.nextLine());
            String valueStr;
            try {
                valueStr = (String) typeReaders.get(String.class).apply(inputLineSc);
            } catch (NoSuchElementException e) {
                if (nullable) {
                    valueStr = null;
                } else {
                    System.out.println("Field cannot be null");
                    continue;
                }
            }
            // Check here...
            if (!nullable && valueStr == null) {
                println("field " + name + " cannot be null");
                continue;
            }

            try {
                value = Enum.valueOf(enumClass, valueStr.toUpperCase());
            } catch (IllegalArgumentException e) {
                println("Enum value not found in");
                println(getAllowedEnumValues(enumClass));
                continue;
            }

            if (inputLineSc.hasNext()) {
                println("Trailing input");
            }

            break;
        }


        return value;
    }

    protected void print(String msg) {
        if (isInteractive) {
            System.out.print(msg);
        }
    }

    protected void println(String msg) {
        if (isInteractive) {
            System.out.println(msg);
        }
    }

    private static String getAllowedEnumValues(Class<?> enumCls) {
        return List.of(enumCls.getEnumConstants()).toString();
    }
}
