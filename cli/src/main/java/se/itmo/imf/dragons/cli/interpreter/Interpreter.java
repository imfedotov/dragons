package se.itmo.imf.dragons.cli.interpreter;


import se.itmo.imf.dragons.client.Client;
import se.itmo.imf.dragons.client.commands.Command;
import se.itmo.imf.dragons.client.commands.CommandFactory;
import se.itmo.imf.dragons.client.commands.NoSuchCommandException;
import se.itmo.imf.dragons.client.commands.TestCommand;
import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.core.entities.dragon.DragonHead;
import se.itmo.imf.dragons.core.entities.dragon.DragonType;

import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Stack;

public class Interpreter {
    Client client;
    private boolean isInteractive;

    CommandFactory commandFactory;
    CommandReader cmdReader;
    Scanner sc;

    Stack<String> callStack;

    /**
     * Create interactive interpreter instance
     * @param client
     */
    public Interpreter(Client client) {
        this(client, System.in, new Stack<>());
        isInteractive = true;
    }

    /**
     * Create non-interactive interpreter instance
     * @param client
     * @param scriptPath
     */
    public Interpreter(Client client, String scriptPath) throws IOException {
        this(client, Files.newInputStream(Paths.get(scriptPath)), new Stack<>());
        isInteractive = false;
    }

    private Interpreter(Client client, InputStream inputStream, Stack<String> callStack) {
        this.client = client;
        this.callStack = callStack;

        commandFactory = new CliCommandFactory();
        sc = new Scanner(inputStream);
        cmdReader = new CommandReader();
    }

    private void loop() {
        while (true) {
            Command cmd;

            try {
                cmd = cmdReader.nextCommand();
            } catch (CommandReader.EndInputException e) {
                println(Messages.BYE);
                return;
            }

            System.out.println("read command " + cmd);

            try {
                if (cmd != null) {
                    cmd.execute();
                    if (cmd.getResult() != null) {
                        println(cmd.getResult());
                    }
                }
            } catch (RuntimeException e) {
                System.err.println("Error: " + e.getMessage());
            }
        }
    }

    public void run() {
        println(Messages.GREETING);
        loop();
    }

    private void runSub(String path) {
        if (callStack.contains(path)) {
            println("Recursion not allowed");
            return;
        }

        callStack.push(path);
        try {
            new Interpreter(client, Files.newInputStream(Paths.get(path)) , callStack).run();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        callStack.pop();
    }

    private void print(Object msg) {
        if (isInteractive) {
            System.out.print(msg);
        }
    }

    private void println(Object msg) {
        if (isInteractive) {
            System.out.println(msg);
        }
    }

    private class CommandReader {
        public Command nextCommand() throws EndInputException {
            print("] ");  // prompt, TODO: make constant?

            String cmdLine;
            try {
                cmdLine = sc.nextLine();
            } catch (NoSuchElementException e) {
                // exit the interpreter somehow...
                throw new EndInputException();
            }
            Scanner cmdLineSc = new Scanner(cmdLine);

            Command cmd;
            try {
                cmd = commandFactory.newCommand(cmdLineSc.next());
            } catch (java.util.NoSuchElementException e) {
                return null;
            } catch (NoSuchCommandException e) {
                System.err.println(e.getMessage());
                return null;
            }
            Object[] args = null;
            try {
                args = readArgs(cmd.getArgTypes(), cmdLineSc);
                System.err.print("Read args: [");
                for (Object arg : args) {
                    System.err.print(arg + ", ");
                }
                System.err.println("]");

                cmd.setArgs(args);

                return cmd;
            } catch (InputMismatchException e) {
                System.err.println(e.getMessage());
            }
            return null;
        }

        private Object[] readArgs(Class<?>[] argTypes, Scanner inlineSc) {
            Object[] args = new Object[argTypes.length];

            // Reading inline arguments
            for (int i = 0; i < argTypes.length; i++) {
                Object arg = null;

                try {
                    if (argTypes[i] == Long.class) {
                        arg = inlineSc.nextLong();
                    } else if (argTypes[i] == String.class) {
                        // TODO: better string handling?
                        arg = inlineSc.nextLine().trim();
                    } else if (argTypes[i] == DragonType.class) {
                        arg = DragonType.valueOf(inlineSc.next().toUpperCase());
                    }
                } catch (IllegalArgumentException e) {
                    throw new InputMismatchException("Incorrect arg type");
                }

                if (arg != null) {
                    args[i] = arg;
                }
            }

            if (inlineSc.hasNext()) {
                throw new InputMismatchException("Extraneous input in command line");
            }

            // Read compound arguments
            for (int i = 0; i < argTypes.length; i++) {
                Object arg;

                if (args[i] != null) {  // Inline arguments are mandatory
                    continue;
                }

                if (argTypes[i] == Dragon.class) {
                    arg = new DragonReader(sc, isInteractive).read();
                } else if (argTypes[i] == DragonHead.class) {
                    arg = new DragonHeadReader(sc, isInteractive).read();
                } else if (argTypes[i] == TestCommand.TestEntity.class) {
                    arg = new TestEntityReader(sc, isInteractive).read();
                } else {
                    throw new InputMismatchException("Unsupported arg type " + argTypes[i]);
                }

                args[i] = arg;
            }

            return args;
        }

        static class EndInputException extends Exception {}
    }

    private class CliCommandFactory implements CommandFactory {
        @Override
        public Command newCommand(String name) throws NoSuchCommandException {
            try {
                return client.getCommandFactory().newCommand(name);
            } catch (NoSuchCommandException e) {
                switch (name) {
                    case "help":
                        return new Command("help") {
                            @Override
                            public void execute() {
                                println(Messages.HELP);
                            }
                        };
                    case "exit":
                        return new Command("exit") {
                            @Override
                            public void execute() {
                                println(Messages.BYE);
                                System.exit(0);
                            }
                        };
                    case "execute_script":
                        return new Command("execute_script", new Class[]{String.class}) {
                            @Override
                            public void execute() {
                                println("executing script " + args[0]);
                                runSub((String) args[0]);
                            }
                        };
                    case "register":
                        return new Command("register", new Class[]{String.class}) {
                            @Override
                            public void execute() {
                                print("password: ");
                                String password = sc.nextLine();
                                client.setCreds((String) args[0], password);
                                client.register();
                            }
                        };
                    case "login":
                        return new Command("login", new Class[]{String.class}) {
                            @Override
                            public void execute() {
                                print("password: ");
                                String password = sc.nextLine();
                                client.setCreds((String) args[0], password);
                                client.testCreds();
                            }
                        };
                    default:
                        throw e;
                }
            }
        }
    }

}
