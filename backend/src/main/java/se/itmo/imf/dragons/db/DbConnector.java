package se.itmo.imf.dragons.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnector {
    public static Connection getConnection(String jdbcUrl, String username, String password) {
        Properties props = new Properties();
        props.setProperty("user", username);
        props.setProperty("password", password);

        try {
            return DriverManager.getConnection(jdbcUrl, props);
        } catch (SQLException e) {
            throw new RuntimeException("Unable to connect to database", e);
        }
    }
}
