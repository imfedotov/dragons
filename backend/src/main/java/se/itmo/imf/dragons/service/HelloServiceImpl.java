package se.itmo.imf.dragons.service;

public class HelloServiceImpl implements HelloService {
    private String name;

    public HelloServiceImpl(String name) {
        this.name = name;
    }

    @Override
    public String sayHello() {
        return "Hello, " + name;
    }
}
