package se.itmo.imf.dragons.service;

public class PingServiceImpl implements PingService {
    @Override
    public String ping() {
        return "pong";
    }
}
