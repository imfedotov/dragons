package se.itmo.imf.dragons.service;

import se.itmo.imf.dragons.core.entities.DragonsInfo;
import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.core.entities.dragon.DragonHead;
import se.itmo.imf.dragons.core.entities.dragon.DragonType;
import se.itmo.imf.dragons.db.Repository;

import java.util.Comparator;
import java.util.List;


public class DefaultDragonService implements DragonService {
    private final DragonsInfo info;
    private final Repository<Dragon> repository;

    public DefaultDragonService(DragonsInfo info, Repository<Dragon> repository) {
        this.info = info;
        this.repository = repository;
    }

    @Override
    public DragonsInfo getInfo() {
        return info;
    }

    @Override
    public List<Dragon> getAll() {
        return repository.getAll();
    }

    @Override
    public Dragon add(Dragon newDragon) {
        info.setCount(info.getCount()+1);
        return repository.add(newDragon);
    }

    @Override
    public Dragon update(long id, Dragon newDragon) {
        return repository.update(id, newDragon);
    }

    @Override
    public Dragon remove(long id) {
        info.setCount(info.getCount()-1);
        return repository.remove(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public Dragon head() {
        return repository.head();
    }

    @Override
    public Dragon removeHead() {
        return repository.pop();
    }

    @Override
    public Dragon addIfMin(Dragon newDragon) {
        Dragon min = getAll().stream().min(Comparator.naturalOrder()).orElse(null);

        if (min == null || min.compareTo(newDragon) > 0) {
            return add(newDragon);
        }

        return newDragon;
    }

    @Override
    public long countLessThanDescription(String description) {
        return repository.findAll(d -> d.getDescription().compareTo(description) < 0).size();
    }

    @Override
    public List<Dragon> filterByType(DragonType dragonType) {
        return repository.findAll(d -> d.getType() == dragonType);
    }

    @Override
    public List<Dragon> filterLessThanHead(DragonHead dragonHead) {
        return repository.findAll(d -> d.getHead() != null && d.getHead().compareTo(dragonHead) < 0);
    }
}
