package se.itmo.imf.dragons.exceptions;

/**
 *  This class represents exceptions that are not useful to the end user, and potentially are critical.
 *  (hence subclassing RuntimeException)
 */
public class InternalException extends RuntimeException {
    public InternalException() {
        super();
    }

    public InternalException(String message) {
        super(message);
    }

    public InternalException(String message, Throwable cause) {
        super(message, cause);
    }

    public InternalException(Throwable cause) {
        super(cause);
    }

    protected InternalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
