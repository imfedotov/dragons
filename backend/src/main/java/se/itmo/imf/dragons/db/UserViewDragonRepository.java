package se.itmo.imf.dragons.db;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.exceptions.UserException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.function.Predicate;


/**
 *  This class merges DAO and an ArrayDeque to form a Repository
 *  All writes are first persisted in the DB, then the in-memory collection is updated.
 *  But we somehow have to make do only with `synchronized`... what a pain
 */
public class UserViewDragonRepository implements Repository<Dragon> {
    private final DequeEntityManager<Long, Dragon> em;

    private final String ownerUsername;

    public UserViewDragonRepository(String ownerUsername, DequeEntityManager<Long, Dragon> em) {
        this.ownerUsername = ownerUsername;
        this.em = em;
    }

    @Override
    public Dragon getById(long id) {
        Dragon dragon = findOne(d -> d.getId() == id);
        if (dragon == null) {
            throw new UserException("Dragon with id " + id + " not found");
        }
        return dragon;
    }

    @Override
    public List<Dragon> getAll() {
        return findAll(foo -> true);
    }

    @Override
    public List<Dragon> findAll(Predicate<Dragon> where) {
        return em.findAll(where);
    }

    @Override
    public Dragon findOne(Predicate<Dragon> where) {
        return em.findAll(where).stream().findFirst().orElse(null);
    }

    @Override
    public Dragon head() {
        Dragon head = em.peekFirst();
        if (head == null) {
            throw new UserException("Collection is empty");
        }
        return head;
    }

    @Override
    public Dragon pop() {
        synchronized (em) {
            Dragon ownFirst = findOne(this::isOwned);
            if (ownFirst == null) {
                throw new UserException("No dragons owned");
            }
            if (em.remove(ownFirst)) {
                return ownFirst;
            } else {
                return null;
            }
        }
    }

    @Override
    public Dragon add(Dragon newDragon) {
        newDragon.setCreationDate(ZonedDateTime.now());
        newDragon.setCreator(ownerUsername);
        if (em.save(newDragon)) {
            return newDragon;
        } else {
            return null;
        }
    }

    @Override
    public Dragon update(long id, Dragon newDragon) {
        synchronized (em) {
            Dragon oldDragon = getById(id);
            if (!isOwned(oldDragon)) {
                throw new UserException("Dragon id " + id + " not owned by you");
            }

            oldDragon.setName(newDragon.getName());
            oldDragon.setAge(newDragon.getAge());
            oldDragon.setCoordinates(newDragon.getCoordinates());
            oldDragon.setDescription(newDragon.getDescription());
            oldDragon.setAge(newDragon.getAge());
            oldDragon.setColor(newDragon.getColor());
            oldDragon.setType(newDragon.getType());
            oldDragon.setHead(newDragon.getHead());

            if (em.update(oldDragon)) {
                return oldDragon;
            } else {
                return null;
            }
        }
    }

    @Override
    public Dragon remove(long id) {
        synchronized (em) {
            Dragon d = getById(id);
            if (!isOwned(d)) {
                throw new UserException("Dragon id " + id + " not owned by you");
            }
            if (em.remove(d)) {
                return d;
            } else {
                return null;
            }
        }
    }

    public List<Dragon> clear() {
        synchronized (em) {
            List<Dragon> owned = em.findAll(this::isOwned);
            if (owned.isEmpty()) {
                throw new UserException("No dragons owned");
            }
            for (Dragon d : owned) {
                em.remove(d);
            }
            return owned;
        }
    }

    private boolean isOwned(Dragon dragon) {
        return dragon.getCreator().equals(ownerUsername);
    }
}
