package se.itmo.imf.dragons.db

;

import java.util.List;

/**
 * SQL DAO
 * @param <K> Key type
 * @param <T> Entity type
 */
public interface Dao<K, T> {
    boolean save(T obj);
    T get(K key);
    boolean update(T obj);
    boolean delete(T obj);
    List<T> getAll();
}