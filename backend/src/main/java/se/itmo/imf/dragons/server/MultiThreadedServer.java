package se.itmo.imf.dragons.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

public class MultiThreadedServer implements Runnable, AutoCloseable {
    private final ServerSocket socket;
    private final ExecutorService sendPool;
    private RequestHandler requestHandler;

    public MultiThreadedServer(int port) throws IOException {
        socket = new ServerSocket(port);
        sendPool = Executors.newFixedThreadPool(16);
    }

    public void setRequestHandler(RequestHandler handler) {
        requestHandler = handler;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Socket sock = socket.accept();
                sock.setSoTimeout(5000);
                new Thread(new SocketHandler(sock)).start();  // 1. Read request in a Thread
            } catch (IOException e) {
                System.err.println("Error accepting client: " + e.getMessage());
            }
        }
    }

    @Override
    public void close() throws Exception {
        if (socket != null) {
            socket.close();
        }
    }

    private class SocketHandler implements Runnable {
        private final Socket sock;

        SocketHandler(Socket sock) {
            this.sock = sock;
        }

        @Override
        public void run() {
            String request;
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                request = reader.readLine();  // (1) right here
            } catch (IOException e) {
                System.err.println("Error reading request from client: " + e.getMessage());
                try {
                    reader.close();
                    sock.close();
                } catch (IOException e1) {
                    // fucking what bitch ass why cant i close it
                }
                return;
            }
            System.out.println("req: " + request);
            System.out.println(sock.isClosed());


            // 2. Process in a separate thread
            AtomicReference<String> responseAtom = new AtomicReference<String>();
            Thread processThread = new Thread(
                   () -> responseAtom.set(requestHandler.handle(request)));
            try {
                processThread.start();
                processThread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            String response = responseAtom.get();

            // 3. Send response via FixedThreadPool
            try {
                PrintStream ps = new PrintStream(sock.getOutputStream());
                sendPool.execute(() -> {
                    System.out.println("resp: " + response);
                    ps.println(response);
                    ps.flush();
                    ps.close();
                });
            } catch (IOException e) {
                System.out.println("Error opening response stream: " + e.getMessage());
            }
        }
    }
}
