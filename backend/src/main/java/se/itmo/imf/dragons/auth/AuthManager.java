package se.itmo.imf.dragons.auth;

import se.itmo.imf.dragons.exceptions.InternalException;
import se.itmo.imf.dragons.exceptions.UserException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

/**
 * This class should do ensureSchema() BEFORE any Dao does theirs.
 */
public class AuthManager {
    private final Connection dbConn;
    private AuthService service = new AuthManagerService();

    private static final String INIT_TABLE = """
        CREATE TABLE IF NOT EXISTS "User" (
            username text PRIMARY KEY,
            passwordHash char(56) NOT NULL
        );
    """;

    private static final String SELECT_USER = """
        SELECT * FROM "User" WHERE username = ? LIMIT 1
    """;

    private static final String INSERT_USER = """
        INSERT INTO "User" (username, passwordHash) VALUES (?, ?)
    """;

    public AuthManager(Connection dbConn) {
        this.dbConn = dbConn;
        ensureSchema();
    }

    public AuthService getService() {
        // This could've returned a singleton but oh who cares, it's lightweight enough.
        return service;
    }

    public synchronized boolean checkAuth(String username, String password) {
        String givenPassHash = hashPassword(username, password);
        String userPassHash = selectUserPass(username);
        return userPassHash != null && safeCompareStrings(givenPassHash, userPassHash);
    }

    private void ensureSchema() {
        try (Statement stmt = dbConn.createStatement()) {
            stmt.executeUpdate(INIT_TABLE);
        } catch (SQLException e) {
            throw new RuntimeException("Unable to init auth table: " + e.getMessage());
        }
    }

    private String selectUserPass(String username) {
        try (PreparedStatement stmt = dbConn.prepareStatement(SELECT_USER)) {
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getString(2);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new InternalException("Unable to select user: " + e.getMessage());
        }
    }

    private void insertUser(String username, String password) {
        String passwordHash = hashPassword(username, password);
        try (PreparedStatement stmt = dbConn.prepareStatement(INSERT_USER)) {
            stmt.setString(1, username);
            stmt.setString(2, passwordHash);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new InternalException("Unable to insert user: " + e.getMessage());
        }
    }

    private static String hashPassword(String username, String password) {
        return hexHashString(username + "$$" + password + "@");
    }

    private static String hexHashString(String str) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-224");
            byte[] hash = digest.digest(str.getBytes(StandardCharsets.UTF_8));
            return bytesToHex(hash);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder(2 * (224 / 8));
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    private static boolean safeCompareStrings(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        }
        int d = 0;
        for (int i = 0; i < a.length(); i++) {
            d |= a.charAt(i) ^ b.charAt(i);
        }
        return (d == 0);
    }

    // Ok maybe this should be a separate class, but it's tightly bound to this AuthManager, and we don't plan on
    // making any other auth managers.
    private class AuthManagerService implements AuthService {
        @Override
        public synchronized void register(String username, String password) {
            String userPassHash = selectUserPass(username);
            if (userPassHash != null) {
                throw new UserException("User already exists");
            }
            insertUser(username, password);
        }
    }
}
