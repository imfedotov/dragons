package se.itmo.imf.dragons.db;

import java.util.ArrayDeque;
import java.util.List;
import java.util.function.Predicate;

/**
 * This class unites ArrayDeque and Dao.
 *
 */
public class DequeEntityManager<K, T> {
    private final Dao<K, T> dao;
    private final ArrayDeque<T> array;

    DequeEntityManager(Dao<K, T> dao) {
        this.dao = dao;
        this.array = new ArrayDeque<>(dao.getAll());
    }

    synchronized List<T> findAll(Predicate<T> where) {
        return array.stream().filter(where).toList();
    }

    synchronized T peekFirst() {
        return array.peekFirst();
    }

    synchronized T pollFirst() {
        return array.pollFirst();
    }

    synchronized boolean save(T obj) {
        if (dao.save(obj)) {
            array.push(obj);
            return true;
        }
        return false;
    }

    synchronized boolean update(T obj) {
        return dao.update(obj);
    }

    synchronized boolean remove(T obj) {
        if (dao.delete(obj)) {
            array.remove(obj);
            return true;
        }
        return false;
    }
}
