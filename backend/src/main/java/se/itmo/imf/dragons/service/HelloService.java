package se.itmo.imf.dragons.service;

public interface HelloService {
    String sayHello();
}
