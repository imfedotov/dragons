package se.itmo.imf.dragons.db;

import java.util.List;
import java.util.function.Predicate;

/**
 * Generic Repository (with long as keys)
 * @param <T> Managed entity type
 */
public interface Repository<T> {
    T getById(long id);

    List<T> getAll();

    List<T> findAll(Predicate<T> where);

    T findOne(Predicate<T> where);

    T head();

    T pop();

    T add(T newElem);

    T update(long id, T newElem);

    T remove(long id);

    List<T> clear();
}
