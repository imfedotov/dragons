package se.itmo.imf.dragons.server;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

class MethodInvoker<T> {
    private final String ifaceName;
    private final T instance;
    private final HashMap<String, Method> methods;

    MethodInvoker(Class<T> iface, T instance) {
        this.ifaceName = iface.getCanonicalName();
        this.instance = instance;
        this.methods = new HashMap<>();

        for (Method method : iface.getMethods()) {
            if (methods.containsKey(method.getName())) {
                throw new RuntimeException("Overloads not supported: " + method.getName() + "(...)");
            }
            method.setAccessible(true);
            methods.put(method.getName(), method);
        }
    }

    MethodInvoker(Class<T> iface) {
        this(iface, null);
    }

    public String getIfaceName() {
        return ifaceName;
    }

    public boolean hasInstance() {
        return instance != null;
    }

    public boolean hasMethod(String methodName) {
        return methods.containsKey(methodName);
    }

    private Method getMethodMust(String methodName) {
        if (!hasMethod(methodName)) {
            throw new RuntimeException("Method " + methodName + " not found");
        }

        return methods.get(methodName);
    }

    public Class<?>[] getParamTypes(String methodName) {
        return getMethodMust(methodName).getParameterTypes();
    }

    protected T getCalleeInstance() {
        return instance;
    }

    public Object call(String methodName, Object... args) throws Throwable {
        return call(getCalleeInstance(), methodName, args);
    }

    public Object call(Object instance, String methodName, Object... args) throws Throwable {
        Object retval = null;
        try {
            retval = getMethodMust(methodName).invoke(instance, args);
        } catch (InvocationTargetException e) {
            throw e.getCause();
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return retval;
    }
}
