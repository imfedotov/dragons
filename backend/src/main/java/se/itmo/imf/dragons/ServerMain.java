package se.itmo.imf.dragons;

import se.itmo.imf.dragons.auth.AuthManager;
import se.itmo.imf.dragons.auth.AuthService;
import se.itmo.imf.dragons.db.DbConnector;
import se.itmo.imf.dragons.jsonrpcv1.JsonRpcRequest;
import se.itmo.imf.dragons.server.JsonRpcHandler;
import se.itmo.imf.dragons.server.MultiThreadedServer;
import se.itmo.imf.dragons.server.RequestHandler;
import se.itmo.imf.dragons.service.*;

import java.io.IOException;
import java.sql.Connection;
import java.util.function.Function;

public class ServerMain {
    private static final String ENV_DB_JDBC_URL = "DB_JDBC_URL";
    private static final String ENV_DB_USER = "DB_USER";
    private static final String ENV_DB_PASS = "DB_PASS";

    private static final String ENV_PORT = "PORT";

    public static void main(String... args) throws InterruptedException {
        MultiThreadedServer server = setupServer();
        server.setRequestHandler(setupRpc());

        Thread serverThread = new Thread(server);
        serverThread.start();
        serverThread.join();
    }

    private static MultiThreadedServer setupServer() {
        String listenPort = requireEnv(ENV_PORT);
        try {
            return new MultiThreadedServer(Integer.parseInt(listenPort));
        } catch (IOException e) {
            System.err.println("Server setup error: " + e.getMessage());
            System.exit(1);
            return null;
        }
    }

    private static AuthManager setupAuth() {
        return new AuthManager(getDatabaseConnection());
    }

    private static RequestHandler setupRpc() {
        AuthManager am = setupAuth();
        DragonServiceFactory.setupFactory(getDatabaseConnection());

        JsonRpcHandler handler = new JsonRpcHandler(am);
        handler.implement(AuthService.class, am.getService(), false);
        handler.implement(PingService.class, new PingServiceImpl(), false);
        handler.implement(HelloService.class,
                (Function<JsonRpcRequest, HelloService>)
                        (JsonRpcRequest req) -> new HelloServiceImpl(req.getAuth().getUsername()),
                true);
        handler.implement(DragonService.class,
                (Function<JsonRpcRequest, DragonService>)
                        (JsonRpcRequest req) -> DragonServiceFactory.newDragonServiceForUser(req.getAuth().getUsername()),
                true);
        return handler;
    }

    private static Connection getDatabaseConnection() {
        String jdbcUrl = requireEnv(ENV_DB_JDBC_URL);
        String user = requireEnv(ENV_DB_USER);
        String pass = requireEnv(ENV_DB_PASS);

        return DbConnector.getConnection(jdbcUrl, user, pass);
    }

    private static String requireEnv(String varName) {
        String val = System.getenv(varName);
        if (val == null) {
            System.err.printf("Please set %s\n", varName);
            System.exit(1);
        }
        return val;
    }
}
