package se.itmo.imf.dragons.auth;

public interface AuthService {
    void register(String username, String password);
}
