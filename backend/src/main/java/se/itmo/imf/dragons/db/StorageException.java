package se.itmo.imf.dragons.db;

import se.itmo.imf.dragons.exceptions.InternalException;

public class StorageException extends InternalException {
}
