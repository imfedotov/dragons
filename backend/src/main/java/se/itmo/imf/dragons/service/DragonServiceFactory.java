package se.itmo.imf.dragons.service;

import se.itmo.imf.dragons.core.entities.DragonsInfo;
import se.itmo.imf.dragons.db.DragonRepositoryFactory;

import java.sql.Connection;

public class DragonServiceFactory {
    private static final DragonsInfo info = new DragonsInfo();
    private static DragonRepositoryFactory factory;

    public static DragonService newDragonServiceForUser(String username) {
        return new DefaultDragonService(info, factory.newUserViewDragonRepository(username));
    }

    /**
     * MUST be called before doing anything with this Factory!
     * @param dbConn
     */
    public static void setupFactory(Connection dbConn) {
        factory = new DragonRepositoryFactory(dbConn);
    }
}
