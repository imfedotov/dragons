package se.itmo.imf.dragons.db;

import se.itmo.imf.dragons.core.entities.dragon.*;

import java.sql.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;


public class DragonDao implements Dao<Long, Dragon> {
    private final Connection dbConn;
    private final SqlFactory sqlFactory = new SqlFactory();

    DragonDao(Connection conn) {
        dbConn = conn;
        ensureSchema();
    }

    private void ensureSchema() {
        try (Statement stmt = dbConn.createStatement()) {
            stmt.executeUpdate(SqlFactory.INIT_TABLES);   
        } catch (SQLException e) {
            throw new RuntimeException("Unable to init Dragon tables", e);
        } 
    }

	@Override
	public boolean save(Dragon dragon) {
        try {
            dbConn.setAutoCommit(false);
            Long headId = null;
            if (dragon.getHead() != null) {
                headId = insertDragonHeadGetId(dragon.getHead());
                // Close this!
                PreparedStatement stmt = sqlFactory.prepareInsertDragon(dragon, headId);
                stmt.executeUpdate();
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                if (generatedKeys.next()) {
                    dragon.setId(generatedKeys.getLong(1));
                    dbConn.commit();
                    return true;
                } else {
                    dbConn.rollback();
                }
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            try {
                dbConn.setAutoCommit(true);
            } catch (SQLException ignored) {
            }
        }
        return false;
    }

    private Long insertDragonHeadGetId(DragonHead dragonHead) {
        try (PreparedStatement stmt = sqlFactory.prepareInsertDragonHead(dragonHead)) {
            stmt.executeUpdate();
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
        return null;
    }

    private DragonHead selectDragonHead(Long id) {
        try (PreparedStatement stmt = sqlFactory.prepareSelectDragonHead(id)) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                DragonHead dragonHead = new DragonHead();
                dragonHead.setSize(rs.getLong(1));
                dragonHead.setEyesCount(rs.getFloat(2));
                return dragonHead;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    private Long selectDragonHeadId(Long id) {
        try (PreparedStatement stmt = sqlFactory.prepareSelectDragonHeadId(id)) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
        return null;
    }

	@Override
	public Dragon get(Long key) {
        if (key == null) {return null;}

        try (PreparedStatement stmt = sqlFactory.prepareSelectDragon(key)) {
            //Long id = null;
            dbConn.setAutoCommit(false);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return readDragon(rs);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            try {
                dbConn.setAutoCommit(true);
            } catch (SQLException ignored) {
            }
        }
	}

	@Override
	public boolean update(Dragon dragon) {
		if (dragon == null) { return false; }
        try (PreparedStatement stmt1 = sqlFactory.preparedUpdateDragon(dragon)) {
            dbConn.setAutoCommit(false);
            Long headId = null;
            if (dragon.getHead() != null) {
                headId = selectDragonHeadId(dragon.getId());
                PreparedStatement stmt2 = sqlFactory.preparedUpdateDragonHead(dragon, headId);
                stmt1.executeUpdate();
                stmt2.executeUpdate();
                dbConn.commit();
                return true;
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            try {
                dbConn.setAutoCommit(true);
            } catch (SQLException e) {
            }
        }
        return false;
	}

	@Override
	public boolean delete(Dragon dragon) {
        if (dragon == null) {return false;}
        Long headId = null;
        try {
            dbConn.setAutoCommit(false);
            headId = selectDragonHeadId(dragon.getId());
            PreparedStatement ps = sqlFactory.preparedDeleteDragon(headId);

            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            try {
                dbConn.setAutoCommit(true);
            } catch (SQLException e) {
            }
        }
    }

	@Override
	public List<Dragon> getAll() {
		ArrayList<Dragon> list = new ArrayList<>();
        try (PreparedStatement stmt = sqlFactory.preparedSelectDragons()) {
            dbConn.setAutoCommit(false);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(readDragon(rs));
            }
            return list;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            try {
                dbConn.setAutoCommit(true);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
	}

    private static ZonedDateTime toZonedDateTime(Struct s) throws SQLException {
        Timestamp ts = (Timestamp) s.getAttributes()[0];
        String tz = (String) s.getAttributes()[1];
        Instant ins = ts.toInstant();
        return ZonedDateTime.now();
        // return ZonedDateTime.ofInstant(ins, ZoneId.of(tz));
    }

    private Dragon readDragon(ResultSet rs) throws SQLException {
        Dragon dragon = new Dragon();
        dragon.setId(rs.getLong("id"));
        dragon.setName(rs.getString("name"));

        Coordinates coordinates = new Coordinates();
        coordinates.setX(rs.getLong("Coordinates_x"));
        coordinates.setY(rs.getFloat("Coordinates_y"));
        dragon.setCoordinates(coordinates);

        ZonedDateTime cDate = ZonedDateTime.ofInstant(
                rs.getTimestamp("ts").toInstant(),
                ZoneId.of(rs.getString("tz"))
        );
        dragon.setCreationDate(cDate);

        dragon.setAge(rs.getLong("age"));
        dragon.setDescription(rs.getString("description"));
        dragon.setColor(Color.valueOf(rs.getString("color")));
        dragon.setType(DragonType.valueOf(rs.getString("type")));

        dragon.setHead(selectDragonHead(rs.getLong("dragonHeadId")));
        dragon.setCreator(rs.getString("creator"));

        return dragon;
    }

    private class SqlFactory {
        static String INIT_TABLES = """
        DO $$
        BEGIN
            IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'ecolor') THEN
                CREATE TYPE EColor AS ENUM ('GREEN', 'BLACK', 'YELLOW');
            END IF;
            IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'edragontype') THEN
                CREATE TYPE EDragonType AS ENUM ('WATER', 'UNDERGROUND', 'AIR', 'FIRE');
            END IF;
            IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'zoneddatetime') THEN
                CREATE TYPE ZonedDateTime AS (ts TIMESTAMP WITH TIME ZONE, tz text);
            END IF;
        END$$;

        CREATE TABLE IF NOT EXISTS DragonHead (
            id serial PRIMARY KEY,
            size bigint NOT NULL,
            eyesCount float
        );

        CREATE TABLE IF NOT EXISTS Dragon (
            id serial PRIMARY KEY,
            name text NOT NULL CHECK(name <> ''),
            Coordinates_x bigint,
            Coordinates_y float CHECK(Coordinates_y > -964),
            creationDate ZonedDateTime NOT NULL,
            age bigint CHECK(age > 0),
            description text,
            color EColor NOT NULL,
            type EDragonType NOT NULL,
            dragonHeadId int REFERENCES DragonHead ON DELETE CASCADE,
            creator text NOT NULL REFERENCES "User"
        );
        """;
        /*
         * creatorId int REFERENCES("User")
         */


        static String SELECT_ALL_DRAGONS = """
            SELECT *, (creationDate).* FROM Dragon
                JOIN DragonHead ON DragonHead.id = Dragon.dragonHeadId
                ORDER BY dragon.id;
        """;

        static String INSERT_DRAGON_HEAD = """
            INSERT INTO DragonHead (size, eyesCount) VALUES (?, ?)
        """;

        static String INSERT_DRAGON = """
            INSERT INTO Dragon (
                name,
                Coordinates_x,
                Coordinates_y,
                creationDate,
                age,
                description,
                color,
                type,
                dragonHeadId,
                creator
            ) VALUES (?, ?, ?, (?, ?), ?, ?, CAST(? AS EColor), CAST(? AS EDragonType), ?, ?)
        """;

        static String UPDATE_DRAGON = """
            UPDATE Dragon SET
                name = ?, Coordinates_x = ?, Coordinates_y = ?, age = ?,
                description = ?, color = CAST(? AS EColor), type = CAST(? AS EDragonType) WHERE id = ?
        """;

        static String UPDATE_DRAGON_HEAD = """
            UPDATE DragonHead SET
                size = ?, eyesCount = ? WHERE id = ?
        """;

        static String DELETE_DRAGON = """
            DELETE FROM DragonHead WHERE id = ?
        """;

        static String SELECT_DRAGON_HEAD_ID = """
            SELECT dragonHeadId FROM Dragon WHERE id = ?
        """;

        static String SELECT_DRAGON_HEAD = """
            SELECT * FROM DragonHead WHERE id = ?
        """;

        static String SELECT_DRAGON = """
             SELECT *, (creationDate).* FROM Dragon WHERE id = ?
        """;

        PreparedStatement prepareInsertDragonHead(DragonHead dragonHead) throws SQLException {
            PreparedStatement stmt = dbConn.prepareStatement(INSERT_DRAGON_HEAD, Statement.RETURN_GENERATED_KEYS);
            stmt.setLong(1, dragonHead.getSize());
            stmt.setFloat(2, dragonHead.getEyesCount());
            return stmt;
        }

        PreparedStatement prepareInsertDragon(Dragon dragon, Long headId) throws SQLException {
            PreparedStatement stmt = dbConn.prepareStatement(INSERT_DRAGON, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, dragon.getName());
            stmt.setLong(2, dragon.getCoordinates().getX());
            stmt.setFloat(3, dragon.getCoordinates().getY());

            stmt.setTimestamp(4, Timestamp.valueOf(dragon.getCreationDate().toLocalDateTime()));
            stmt.setString(5, dragon.getCreationDate().getZone().getId());

            stmt.setLong(6, dragon.getAge());
            stmt.setString(7, dragon.getDescription());
            stmt.setString(8, dragon.getColor().toString());
            stmt.setString(9, dragon.getType().toString());
            stmt.setLong(10, headId);
            stmt.setString(11, dragon.getCreator());
            return stmt;
        }

        PreparedStatement prepareSelectDragonHead(Long id) throws SQLException {
            PreparedStatement stmt = dbConn.prepareStatement(SELECT_DRAGON_HEAD);
            stmt.setLong(1, id);
            return stmt;
        }

        PreparedStatement prepareSelectDragon(Long id) throws SQLException {
            PreparedStatement stmt = dbConn.prepareStatement(SELECT_DRAGON);
            stmt.setLong(1, id);
            return stmt;
        }

        PreparedStatement prepareSelectDragonHeadId(Long id) throws SQLException {
            PreparedStatement stmt = dbConn.prepareStatement(SELECT_DRAGON_HEAD_ID);
            stmt.setLong(1, id);
            return stmt;
        }

        PreparedStatement preparedUpdateDragon(Dragon dragon) throws SQLException {
            PreparedStatement stmt = dbConn.prepareStatement(UPDATE_DRAGON);
            stmt.setString(1, dragon.getName());
            stmt.setLong(2, dragon.getCoordinates().getX());
            stmt.setFloat(3, dragon.getCoordinates().getY());
            stmt.setLong(4, dragon.getAge());
            stmt.setString(5, dragon.getDescription());
            stmt.setString(6, dragon.getColor().toString());
            stmt.setString(7, dragon.getType().toString());
            stmt.setLong(8, dragon.getId());
            return stmt;
        }

        PreparedStatement preparedUpdateDragonHead(Dragon dragon, Long headId) throws SQLException {
            PreparedStatement stmt = dbConn.prepareStatement(UPDATE_DRAGON_HEAD);
            stmt.setLong(1, dragon.getHead().getSize());
            stmt.setFloat(2, dragon.getHead().getEyesCount());
            stmt.setFloat(3, headId);
            return stmt;
        }

        PreparedStatement preparedDeleteDragon(Long headId) throws SQLException {
            PreparedStatement stmt = dbConn.prepareStatement(DELETE_DRAGON);
            stmt.setLong(1, headId);
            return stmt;
        }

        PreparedStatement preparedSelectDragons() throws SQLException {
            PreparedStatement stmt = dbConn.prepareStatement(SELECT_ALL_DRAGONS);
            return stmt;
        }
    }
}

/*
CREATE TYPE COLOR AS ENUM ('GREEN', 'BLACK', 'YELLOW');
CREATE TYPE DRAGON_TYPE AS ENUM ('WATER', 'UNDERGROUND', 'AIR', 'FIRE');

CREATE TABLE "User" (
  id SERIAL PRIMARY KEY,
  login VARCHAR(50),
  password VARCHAR(50)
);

CREATE TABLE DragonHead (
  id SERIAL PRIMARY KEY,
  size BIGINT NOT NULL,
  eyes_count REAL NOT NULL
);

CREATE TABLE Dragon (
  id SERIAL PRIMARY KEY,
  creator_id SERIAL REFERENCES "User",
  name VARCHAR(255) NOT NULL,
  x BIGINT NOT NULL,
  y REAL NOT NULL,
  creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
  age BIGINT NOT NULL,
  description VARCHAR(255),
  color COLOR NOT NULL,
  type DRAGON_TYPE NOT NULL,
  head_id SERIAL REFERENCES DragonHead ON DELETE CASCADE
);
 */
