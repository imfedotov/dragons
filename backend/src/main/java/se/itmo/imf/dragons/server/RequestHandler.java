package se.itmo.imf.dragons.server;

public interface RequestHandler {
    String handle(String request);
}
