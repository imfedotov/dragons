package se.itmo.imf.dragons.db;

import se.itmo.imf.dragons.core.entities.dragon.Dragon;

import java.sql.Connection;

public class DragonRepositoryFactory {
    private final Dao<Long, Dragon> dragonDao;
    private final DequeEntityManager<Long, Dragon> dragonEm;

    public DragonRepositoryFactory(Connection dbConn) {
        dragonDao = new DragonDao(dbConn);
        dragonEm = new DequeEntityManager<>(dragonDao);
    }

    public Repository<Dragon> newUserViewDragonRepository(String username) {
        return new UserViewDragonRepository(username, dragonEm);
    }
}
