package se.itmo.imf.dragons.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import se.itmo.imf.dragons.auth.AuthManager;
import se.itmo.imf.dragons.exceptions.UserException;
import se.itmo.imf.dragons.jsonrpcv1.JsonRpcRequest;
import se.itmo.imf.dragons.jsonrpcv1.JsonRpcResponse;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JsonRpcHandler implements RequestHandler {
    private static final ObjectMapper jsonMapper;
    private static final Logger LOGGER = Logger.getLogger(JsonRpcHandler.class.getName());

    private final AuthManager authManager;
    private final LinkedList<MethodInvokerDescriptor> invokerDescriptors;

    static {
        jsonMapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(MapperFeature.ALLOW_COERCION_OF_SCALARS, false)
                .build();
        // TODO: copy this to client code... possibly move up there!
    }

    public JsonRpcHandler(AuthManager authManager) {
        this.authManager = authManager;
        invokerDescriptors = new LinkedList<>();
    }

    /**
     * Implement an interface in this JsonRpcHandler, creating a new instance with ctor for each method call
     * @param iface Interface to implement. Overloads not allowed
     * @param ctor Function that returns an interface implementor given the Request
     * @param <T> Interface type
     */
    public <T> void implement(Class<T> iface, Function<JsonRpcRequest, T> ctor, boolean requireAuth) {
        MethodInvoker<T> invoker = new MethodInvoker<>(iface);
        MethodInvokerDescriptor desc = new MethodInvokerDescriptor(invoker, ctor, requireAuth);
        if (invokerDescriptors.stream().anyMatch(d -> d.mi.getIfaceName().equals(invoker.getIfaceName()))) {
            throw new IllegalArgumentException("Iface '" + invoker.getIfaceName() + "' already implemented");
        }
        invokerDescriptors.push(desc);
    }

    /**
     * Implement an interface in this JsonRpcHandler, calling methods of obj
     * @param iface Interface to implement. Overloads not supported
     * @param obj Callee instance
     * @param <T> Interface type
     */
    public <T> void implement(Class<T> iface, T obj, boolean requireAuth) {
        MethodInvoker<T> invoker = new MethodInvoker<>(iface, obj);
        MethodInvokerDescriptor desc = new MethodInvokerDescriptor(invoker, null, requireAuth);
        if (invokerDescriptors.stream().anyMatch(d -> d.mi.getIfaceName().equals(invoker.getIfaceName()))) {
            throw new IllegalArgumentException("Iface '" + invoker.getIfaceName() + "' already implemented");
        }
        invokerDescriptors.push(desc);
    }

    @Override
    public String handle(String requestJson)  {
        JsonRpcRequest request;
        try {
            request = parseRequest(requestJson);
        } catch (JsonProcessingException jpe) {
            throw new UserException("JSON parse error");
        } catch (UserException ue) {
            JsonRpcResponse resp = new JsonRpcResponse();
            resp.setId(Long.MIN_VALUE);
            resp.setError(ue.getMessage());
            return dumpResponse(resp);
        }

        JsonRpcResponse resp = new JsonRpcResponse();
        resp.setId(request.getId());

        MethodInvokerDescriptor desc = getMethodInvokerDescriptor(request.getMethod());
        if (desc.requiresAuth) {
            if (request.getAuth() == null) {
                resp.setError("Auth required");
                return dumpResponse(resp);
            }
            if (!authManager.checkAuth(request.getAuth().getUsername(), request.getAuth().getPassword())) {
                resp.setError("Wrong username or password");
                return dumpResponse(resp);
            }
        }

        try {
            Object result;
            if (desc.ctor != null) {
                Object callee = desc.ctor.apply(request);
                result = desc.mi.call(callee, request.getMethod(), request.getParams());
            } else {
                result = desc.mi.call(request.getMethod(), request.getParams());
            }
            resp.setResult(result);
        } catch (UserException ue) {
            resp.setError(ue.getMessage());
        } catch (Throwable te) {
            te.printStackTrace();
            resp.setError("Internal error");
        }

        return dumpResponse(resp);
    }

    private MethodInvokerDescriptor getMethodInvokerDescriptor(String methodName) {
        return invokerDescriptors.stream().filter(d -> d.mi.hasMethod(methodName)).findFirst().orElse(null);
    }

    private JsonRpcRequest parseRequest(String request) throws JsonProcessingException {
        JsonNode root = jsonMapper.readTree(request);
        JsonRpcRequest.Auth auth = jsonMapper.treeToValue(root.path("auth"), JsonRpcRequest.Auth.class);

        long id = root.path("id").asLong();
        String methodName = root.path("method").asText();

        MethodInvokerDescriptor desc = getMethodInvokerDescriptor(methodName);
        if (desc == null) {
            throw new UserException("Method '" + methodName + "' not found");
        }

        Class<?>[] paramTypes = desc.mi.getParamTypes(methodName);
        Iterator<JsonNode> paramsJson = root.path("params").elements();
        Object[] params = new Object[paramTypes.length];
        for (int i = 0; i < paramTypes.length; i++) {
            try {
                params[i] = jsonMapper.treeToValue(paramsJson.next(), paramTypes[i]);
            } catch (JsonProcessingException e) {
                throw new UserException("Invalid parameter " + i + " type");
            }
        }

        JsonRpcRequest req = new JsonRpcRequest();
        req.setMethod(methodName);
        req.setId(id);
        req.setParams(params);
        req.setAuth(auth);
        return req;
    }

    private String dumpResponse(JsonRpcResponse response) {
        JsonNode jsonTree = jsonMapper.valueToTree(response);
        try {
            return jsonMapper.writeValueAsString(jsonTree);
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.WARNING, e.toString());
            throw new RuntimeException(e);
        }
    }

    private static class MethodInvokerDescriptor {
        public final MethodInvoker<?> mi;
        public final Function<JsonRpcRequest, ?> ctor;
        public final boolean requiresAuth;

        private MethodInvokerDescriptor(MethodInvoker<?> mi, Function<JsonRpcRequest, ?> ctor, boolean requiresAuth) {
            this.mi = mi;
            this.ctor = ctor;
            this.requiresAuth = requiresAuth;
        }
    }
}
