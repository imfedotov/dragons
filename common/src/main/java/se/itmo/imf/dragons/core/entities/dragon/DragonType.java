package se.itmo.imf.dragons.core.entities.dragon;

public enum DragonType {
    WATER,
    UNDERGROUND,
    AIR,
    FIRE
}
