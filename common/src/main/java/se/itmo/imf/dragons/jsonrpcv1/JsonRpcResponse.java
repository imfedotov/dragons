package se.itmo.imf.dragons.jsonrpcv1;

public final class JsonRpcResponse {
    private Object result;
    private String error;
    private long id;

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        if (result != null) {
            error = null;
        }
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        if (error != null) {
            result = null;
        }
        this.error = error;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "JsonRpcResponse{" +
                "result=" + result +
                ", error='" + error + '\'' +
                ", id=" + id +
                '}';
    }
}
