package se.itmo.imf.dragons.core.entities.dragon;

public class Dragon implements Comparable<Dragon> {
    private long id; //Значение поля должно быть больше 0, Значение этого поля должно быть уникальным, Значение этого поля должно генерироваться автоматически
    private String name; //Поле не может быть null, Строка не может быть пустой
    private Coordinates coordinates; //Поле не может быть null
    private java.time.ZonedDateTime creationDate; //Поле не может быть null, Значение этого поля должно генерироваться автоматически
    private long age; //Значение поля должно быть больше 0
    private String description; //Поле может быть null
    private Color color; //Поле не может быть null
    private DragonType type; //Поле не может быть null
    private DragonHead head;
    private String creator;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public java.time.ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(java.time.ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public DragonType getType() {
        return type;
    }

    public void setType(DragonType type) {
        this.type = type;
    }

    public DragonHead getHead() {
        return head;
    }

    public void setHead(DragonHead head) {
        this.head = head;
    }
    public String getCreator() {return this.creator;}
    public void setCreator(String creator) {this.creator=creator;}

    public Dragon() {
        this.id = -1;
    }

    @Override
    public String toString() {
        return "Dragon{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", coordinates=" + coordinates +
               ", creationDate=" + creationDate +
               ", age=" + age +
               ", description='" + description + '\'' +
               ", color=" + color +
               ", type=" + type +
               ", head=" + head +
               ", creator='" + creator + '\'' +
               '}';
    }

    @Override
    public int compareTo(Dragon that) {
        int nameComparison = this.name.compareTo(that.name);
        if (nameComparison != 0) {
            return nameComparison < 0 ? -1 : 1;
        }

        if (this.age != that.age) {
            return (this.age < that.age ? -1 : 1);
        }

        if (this.description == null && that.description == null) {
            // pass
        } else if (this.description == null) {
            return -1;
        } else if (that.description == null) {
            return 1;
        } else {
            int descriptionComparison = this.description.compareTo(that.description);
            if (descriptionComparison != 0) {
                return descriptionComparison < 0 ? -1 : 1;
            }
        }

        if (this.head == null && that.head == null) {
            return 0;
        } else if (this.head == null) {
            return -1;
        } else if (that.head == null) {
            return 1;
        } else {
            return this.head.compareTo(that.head);
        }
    }
}
