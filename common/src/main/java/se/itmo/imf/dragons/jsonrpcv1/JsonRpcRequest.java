package se.itmo.imf.dragons.jsonrpcv1;

import java.util.Arrays;

public final class JsonRpcRequest {
    private Auth auth;
    private String method;
    private Object[] params;
    private long id;

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "JsonRpcRequest{" +
                "method='" + method + '\'' +
                ", params=" + Arrays.toString(params) +
                ", id=" + id +
                '}';
    }

    public static class Auth {
        private String username;
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
