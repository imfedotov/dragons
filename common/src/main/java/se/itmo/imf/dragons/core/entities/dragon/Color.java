package se.itmo.imf.dragons.core.entities.dragon;

public enum Color {
    GREEN, 
    BLACK, 
    YELLOW
}
