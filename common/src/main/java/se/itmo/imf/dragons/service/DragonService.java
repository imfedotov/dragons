package se.itmo.imf.dragons.service;

import se.itmo.imf.dragons.core.entities.DragonsInfo;
import se.itmo.imf.dragons.core.entities.dragon.Dragon;
import se.itmo.imf.dragons.core.entities.dragon.DragonHead;
import se.itmo.imf.dragons.core.entities.dragon.DragonType;

import java.util.List;

public interface DragonService {
    DragonsInfo getInfo();

    List<Dragon> getAll();

    Dragon add(Dragon newDragon);

    Dragon update(long id, Dragon newDragon);

    Dragon remove(long id);

    void clear();

    // this is probably a CLI/UI Command
//    void save();

    Dragon head();

    Dragon removeHead();

    Dragon addIfMin(Dragon newDragon);

    long countLessThanDescription(String description);

    List<Dragon> filterByType(DragonType dragonType);

    List<Dragon> filterLessThanHead(DragonHead dragonHead);
}
