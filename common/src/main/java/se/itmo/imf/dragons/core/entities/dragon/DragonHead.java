package se.itmo.imf.dragons.core.entities.dragon;

public class DragonHead implements Comparable<DragonHead> {
    private Long size;  // NotNull
    private float eyesCount;

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public float getEyesCount() {
        return eyesCount;
    }

    public void setEyesCount(float eyesCount) {
        this.eyesCount = eyesCount;
    }

    @Override
    public String toString() {
        return "DragonHead{" +
                "size=" + size +
                ", eyesCount=" + eyesCount +
                '}';
    }

    @Override
    public int compareTo(DragonHead other) {
        final float EPS = 1e-5f;
        float idxThis = size * eyesCount * 0.2f,
            idxThat = other.size + other.eyesCount * 0.2f;

        return (Math.abs(idxThis - idxThat) < EPS) ? 0 : (idxThis < idxThat) ? -1 : 1;
    }
}
