package se.itmo.imf.dragons.core.entities;

import java.time.ZonedDateTime;

public class DragonsInfo {
    private String entityType = "Dragon";
    private long count;
    private long lastId;
    private java.time.ZonedDateTime createdTime;
    private java.time.ZonedDateTime lastSaved;

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public ZonedDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(ZonedDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public ZonedDateTime getLastSaved() {
        return lastSaved;
    }

    public void setLastSaved(ZonedDateTime lastSaved) {
        this.lastSaved = lastSaved;
    }

    public long getLastId() {
        return lastId;
    }

    public void setLastId(long lastId) {
        this.lastId = lastId;
    }

    @Override
    public String toString() {
        return "DragonsInfo{" +
                "entityType='" + entityType + '\'' +
                ", count=" + count +
                ", createdTime=" + createdTime +
                ", lastSaved=" + lastSaved +
                '}';
    }
}
